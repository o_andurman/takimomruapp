import { API_BASE } from '../config';
import axios from 'axios';

import AuthStore from '../store/AuthStore';

class HttpService {
    
    async auth( username, password ){
        try {
            const { data } = await axios.post(`${API_BASE}/api/Login/Accesstoken`, { username, password});
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log("Couldn't authenticate.")
            }
            return false
        }
    }

    async addUser( firstName, lastName, password, company, email, username, companyLogo ){
        
        const formData = new FormData();
        formData.append('firstName', firstName);
        formData.append('lastName', lastName);
        formData.append('password', password);
        formData.append('company', company);
        formData.append('email', email);
        formData.append('username', username);
        formData.append('file', companyLogo);
        const config = {
            headers: {
                'content-type': 'multipart/form-data', 
            }
        }
        try {
            const { data } = await axios.post(`${API_BASE}/api/prod/upload`, formData, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async getMachines() {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.get(`${API_BASE}/api/prod/makinalistele`, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async getCategories( id ) {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.get(`${API_BASE}/api/prod/parcadurum/${id}`, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async addCategory( pieceName, fkDeviceId ) {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.post(`${API_BASE}/api/prod/takimekle`, { pieceName, fkDeviceId }, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async deleteCategory( id ) {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.delete(`${API_BASE}/api/prod/durumsil/${id}`, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async getSubCategories( pieceID ) {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.get(`${API_BASE}/api/prod/parcalistele/${pieceID}`, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async addSubCategory ( subPieceName, toolLife, fkpieceID, type ) {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.post(`${API_BASE}/api/prod/parcaekle`, { subPieceName, toolLife, fkpieceID, type}, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async deleteSubCategory( id ) {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.delete(`${API_BASE}/api/prod/parcasil/${id}`, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async getSubCategoryDetail( subPieceID ) {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.get(`${API_BASE}/api/prod/detaylist/${subPieceID}`, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async getHealth( pieceID ) {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.get(`${API_BASE}/api/prod/parcadurum/${pieceID}`, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async getGoodPiece( pieceID ) {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.get(`${API_BASE}/api/prod/parcayesil/${pieceID}`, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async getNormalPiece( pieceID ) {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.get(`${API_BASE}/api/prod/parcasari/${pieceID}`, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async getCriticPiece( pieceID ) {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.get(`${API_BASE}/api/prod/parcakirmizi/${pieceID}`, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async getNotifications() {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.get(`${API_BASE}/api/prod/bildirimler`, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async deleteNotification(id) {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.delete(`${API_BASE}/api/prod/bildirimsil/${id}`, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async getAccount() {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.get(`${API_BASE}/api/prod/hesap`, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async updateAccount ( firstName, lastName, company, email, username ) {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.put(`${API_BASE}/api/prod/hesapguncelle`, { firstName, lastName, company, email, username }, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async addPieceNote ( noteHeader, noteDescription, fkPieceId, pieceName, type ) {
        // type: false - takıma ait notlar, true - parçaya ait notlar
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.post(`${API_BASE}/api/prod/notekle`, { noteHeader, noteDescription, fkPieceId, pieceName, type }, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async addSubPieceNote ( noteHeader, noteDescription, fkSubPieceId, SubPieceName, type ) {
        // type: false - takıma ait notlar, true - parçaya ait notlar
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.post(`${API_BASE}/api/prod/notekle`, { noteHeader, noteDescription, fkSubPieceId, SubPieceName, type }, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async updateNote ( noteId, noteHeader, noteDescription, id, type ) {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.put(`${API_BASE}/api/prod/notguncelle/${noteId}`, { noteHeader, noteDescription, id, type}, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async deleteNote ( noteId) {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.delete(`${API_BASE}/api/prod/notsil/${noteId}`, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }

    async getNote () {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + AuthStore.token
            }
        }
        try {
            const { data } = await axios.get(`${API_BASE}/api/prod/notlistele`, config);
            return data;
        }catch (e) {
            if(e == "Error: Network Error"){
                alert("İnternet bağlantısı yok.")
            }else{
                console.log(e)
            }
            return false
        }
    }
}

export default new HttpService();
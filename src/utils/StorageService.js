import AsyncStorage from '@react-native-community/async-storage';

class StorageService {

    async saveItem( key, value ){
        try{
            await AsyncStorage.setItem( key, value );
        }catch (e){
            console.log("Couldn't save value.")
        }
    }

    async removeItem ( key ) {
        try{
            await AsyncStorage.removeItem( key );
        }catch (e){
            console.log("Couldn't remove item.")
        }
    }

    async recieveItem( key ){
        try{
            const value = await AsyncStorage.getItem( key );
            return value;
        }catch (e){
            console.log("Couldn't recieve value.")
        }
    }
}

export default new StorageService();
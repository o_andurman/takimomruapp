import React from 'react';

import {
  createAppContainer,
  createSwitchNavigator
} from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';

// Loading screen
import Loading from './screens/Loading';

// auth stack
import SignIn from './screens/SignIn';
import SignUp from './screens/SignUp';

// app stack
import Home from './screens/Home';
import Tools from './screens/Tools';
import Notifications from './screens/Notifications';
import Help from './screens/Help';
import Profile from './screens/Profile';
import Machines from './screens/Machines';
import Report from './screens/Report';

const AuthStack = createStackNavigator(
	{
		SignIn: {
			screen: SignIn,
			navigationOptions: {
				title: null,
				headerTransparent: true,
			}
		},
		SignUp: {
			screen: SignUp,
			navigationOptions: {
				title: null,
				headerTransparent: true,
				headerBackTitleStyle: {color: 'white'},
				headerTintColor: 'white'
			}
		}
  },
  {
		initialRouteName: 'SignIn',
		defaultNavigationOptions: {
			headerBackTitle: 'Geri',
		}
	}
);

const AppStack = createStackNavigator(
	{
		Machines: {
			screen: Machines,
			navigationOptions: {
				title: 'Makineler',
				headerTransparent: true,
				headerTitleStyle: {color: 'white'},
			}
		},
		Home: {
			screen: Home,
			navigationOptions: {
				headerTransparent: true,
				headerTitleStyle: {color: 'white'},
				headerBackTitleStyle: {color: 'white'},
				headerTintColor: 'white'
			}
		},
		Tools: {
			screen: Tools,
			navigationOptions: {
				headerTransparent: true,
				headerTitleStyle: {color: 'white'},
				headerBackTitleStyle: {color: 'white'},
				headerTintColor: 'white'
			}
		},
		Notifications: {
			screen: Notifications,
			navigationOptions: {
				title: 'Bildirimler',
				headerTransparent: true,
				headerTitleStyle: {color: 'white'},
				headerBackTitleStyle: {color: 'white'},
				headerTintColor: 'white'
			}
		},
		Profile: {
			screen: Profile,
			navigationOptions: {
				title: 'Profil',
				headerTransparent: true,
				headerTitleStyle: {color: 'white'},
				headerBackTitleStyle: {color: 'white'},
				headerTintColor: 'white'
			}
		},
		Help: {
			screen: Help,
			navigationOptions: {
				title: 'Yardım',
				headerTransparent: true,
				headerTitleStyle: {color: 'white'},
				headerBackTitleStyle: {color: 'white'},
				headerTintColor: 'white'
			}
		},
		Report: {
			screen: Report,
			navigationOptions: {
				title: 'Rapor',
				headerTransparent: true,
				headerTitleStyle: {color: 'white'},
				headerBackTitleStyle: {color: 'white'},
				headerTintColor: 'white'
			}
		}
	},
	{
		initialRouteName: 'Machines',
		defaultNavigationOptions: {
			headerBackTitle: 'Geri',
			headerTitleAlign: 'center'
		},
	}
);

const SwitchNavigator = createSwitchNavigator(
	{
		AuthLoading: {
			screen: Loading
		},
		App: AppStack,
		Auth: AuthStack,
	},
	{
		initialRouteName: 'AuthLoading',
	}
);

export default createAppContainer(SwitchNavigator);
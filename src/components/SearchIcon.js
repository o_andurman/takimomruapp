import * as React from 'react';
import { TouchableWithoutFeedback } from "react-native";
import { Block, Icon, theme } from 'galio-framework';

export default class SearchIcon extends React.Component {
    constructor(props){
		super(props);
	}
    render() {
      return (
        <TouchableWithoutFeedback
            onPress={() => this.props.formSearchActive()}
        >
            <Block>
                <Icon size={25} color={theme.COLORS.WHITE} style={{marginRight:theme.SIZES.BASE / 6}} name="search" family="font-awesome" />
            </Block>
        </TouchableWithoutFeedback>
      );
    }
}
import * as React from 'react';
import { TouchableWithoutFeedback } from "react-native";
import { Block, Icon, theme } from 'galio-framework';
import NavigationService from '../NavigationService';

const ProfileIcon = () => (
    <TouchableWithoutFeedback
        onPress={() => NavigationService.navigate('Profile')}
    >
        <Block>
            <Icon size={30} color={theme.COLORS.WHITE} style={{marginLeft:theme.SIZES.BASE}} name="user-circle" family="font-awesome" />
        </Block>
    </TouchableWithoutFeedback>
);

export default ProfileIcon;
import * as React from 'react';
import { TouchableWithoutFeedback } from "react-native";
import { Block, Icon, theme } from 'galio-framework';
import { Badge } from 'react-native-paper';
import NavigationService from '../NavigationService';
import HttpService from '../utils/HttpService';

export default class NotificationIcon extends React.Component {
    constructor(props){
        super(props);
        this.state = {
          count: null
        } 
    }

    componentDidMount() {
        this._retrieveNotifications()
    }

    _retrieveNotifications = async () => {
        const result = await HttpService.getNotifications();
        if(!result || result == false || result == []) {
            return false
        }else{
            this.setState({
                count: result[0].count
            })
        }
    } 

    render() {
        if(this.state.count == null || this.state.count == 0)
        {
            return (
                <TouchableWithoutFeedback
                    onPress={() => NavigationService.navigate('Notifications')}
                >
                    <Block row>
                        <Icon size={25} color={theme.COLORS.WHITE} style={{marginRight:theme.SIZES.BASE}} name="bell" family="font-awesome" />
                    </Block>
                </TouchableWithoutFeedback>
            );
        }else{
            return (
                <TouchableWithoutFeedback
                    onPress={() => NavigationService.navigate('Notifications')}
                >
                    <Block row>
                        <Badge style={{ marginTop: -10, marginRight: -10, marginBottom: 14, zIndex: 999 }}>{this.state.count}</Badge>
                        <Icon size={25} color={theme.COLORS.WHITE} style={{marginRight:theme.SIZES.BASE}} name="bell" family="font-awesome" />
                    </Block>
                </TouchableWithoutFeedback>
            );
        }
        
    }
}
import React, { Component } from 'react';
import { TouchableWithoutFeedback, Alert } from "react-native";
import { Block, Icon, theme } from 'galio-framework';
import { SCLAlert, SCLAlertButton } from 'react-native-scl-alert'

import {inject} from 'mobx-react';

@inject('AuthStore')
export default class LogoutButton extends Component {
    state = {
        show: false
    }

    handleOpen = () => {
        this.setState({ show: true })
    }
     
    handleClose = () => {
        this.setState({ show: false })
    }

    render() {
        return (
            <TouchableWithoutFeedback
                onPress={this.handleOpen}
            >
                <Block>
                    <Block row>
                        <Icon size={25} color={theme.COLORS.WHITE} style={{marginRight:theme.SIZES.BASE}} name="power-off" family="font-awesome" />
                    </Block>
                    <SCLAlert
                    show={this.state.show}
                    onRequestClose={this.handleClose}
                    theme="info"
                    title="Çıkış"
                    subtitle="Çıkış yapmak istediğinizden emin misiniz?"
                    headerIconComponent={<Icon name="info" family="font-awesome" size={64} color="white" />}
                    >
                        <SCLAlertButton theme="default" onPress={() => {this.props.AuthStore.removeToken();this.handleClose}}>Çıkış Yap</SCLAlertButton>
                        <SCLAlertButton theme="info" onPress={this.handleClose}>Vazgeç</SCLAlertButton>
                    </SCLAlert>
                </Block>
                
            </TouchableWithoutFeedback>
        );
    }
}
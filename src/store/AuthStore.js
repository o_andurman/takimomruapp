import { observable, action } from 'mobx';
import StorageService from '../utils/StorageService';
import NavigationService from '../NavigationService';

class UserStore{
    @observable token = null;
    @observable user = null;

    @action async saveToken(token, user){
        try{
            await StorageService.saveItem('token',token);
            await StorageService.saveItem('user',user);
            await this.setupAuth();
        }catch (e) {
            console.log("Couldn't reach local storage to save token.");
        }
    }
    
    @action async removeToken() {
        try{
            await StorageService.removeItem('token');
            await StorageService.removeItem('user');
            this.token = null;
            this.user = null;
            await this.setupAuth();
        }catch (e) {
            console.log(e);
        }
    }

    @action async setupAuth(){
        await this.getToken();
    }

    @action async getToken(){
        try{
            const token = await StorageService.recieveItem('token');
            if (!token) {
                NavigationService.navigate('Auth');
                return false
            }
            await this.getUser()
            this.token = token;
            NavigationService.navigate('App');
        }catch (e){
            console.log("Couldn't get token.")
        }
    }

    @action async getUser(){
        try{
            const user = await StorageService.recieveItem('user');
            if (!user) {
                return false
            }
            this.user = user;
        }catch (e){
            console.log("Couldn't get user.")
        }
    }

    @action async setUser(username){
        this.user = username;
        return false
    }
}

export default new UserStore();
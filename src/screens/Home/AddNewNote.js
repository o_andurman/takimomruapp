import React, { Component } from 'react';

import { StyleSheet, Dimensions, KeyboardAvoidingView, Alert } from "react-native";
import { Block, Text, theme, Input, Button, Icon } from 'galio-framework';
import { materialTheme } from '../../constants/';
import HttpService from '../../utils/HttpService';
import { ActivityIndicator, Colors, Divider, Card } from 'react-native-paper';

const { width } = Dimensions.get('screen');

export default class AddNewNote extends Component {
	constructor(props){
		super(props);
		this.state = {
			noteHeader: null,
			noteDescription: null,
			isSubmitting: false,
			active: {
				noteHeader: false,
				noteDescription: false,
			},
			validation: {
				noteHeader: null,
			}
		}
	}
	
	_handleSubmit = async () => {
		try {
			this.setSubmitting();
			let response = await HttpService.addPieceNote(this.state.noteHeader, this.state.noteDescription, this.props.activePieceId, this.props.activePieceName, false );
			if(!response)
			{
				alert("Not eklenirken hata oluştu.")
				this.setSubmitting();
				return false
			}else{
				Alert.alert(   
					'Tebrikler'
					,'Notunuz başarıyla eklendi.'
					,[
						{text: 'Tamam', onPress: () => {
							this.props.retrieveCategories();
							this.props.noteFormVisible();
							} 
						},
					]
				)
			}
		}catch (e) {
			console.log("Couldn't add note.");
		}
	};

	setSubmitting = () => {
		this.setState({isSubmitting: !this.state.isSubmitting})
	}

	handleChange = (name, value) => {
		this.setState({ [name]: value });
		/*const { validation } = this.state;
		if(name=="pieceName")
		{
			//let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			let reg = /^[a-zA-Z0-9 ğçşüöıĞÇŞÜÖİ.-]+$/;
			if(value == '')
			{
				validation["pieceName"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["pieceName"] = "*Bu alan sadece a-z, A-Z veya 0-9 karakterlerini içermelidir.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["pieceName"] = null;
				this.setState({ validation })
			}
		}*/
	}

	toggleActive = (name) => {
		const { active } = this.state;
		active[name] = !active[name];

		this.setState({ active });
	}

	render() {
		return (
			<Block style={[styles.cart]}>
				<Block  middle>
					<KeyboardAvoidingView behavior="padding" enabled>
						<Block row middle style={{ paddingVertical: theme.SIZES.BASE * 0.625}}>
							<Icon name="clipboard" family="font-awesome" size={20} color="white" style={{marginRight:5}}/>
							<Text center color="white" size={theme.SIZES.FONT * 1.5} >
								{this.props.activePieceName} için Not Ekle
							</Text>
						</Block>
						<Divider style={{backgroundColor:'white'}}/>
						<Block >
							<Block middle>
								<Text
								color={theme.COLORS.WHITE}
								size={theme.SIZES.FONT }
								style={{ alignSelf: 'flex-start', lineHeight: theme.SIZES.FONT * 2 }}
								>
									Not Başlığı:
								</Text>
								<Input
								color="white"
                                placeholder="Not Başlığını Giriniz"
								autoCapitalize="none"
								autoCorrect={false}
								bgColor='transparent'
								onBlur={() => this.toggleActive('noteHeader')}
								onFocus={() => this.toggleActive('noteHeader')}
								placeholderTextColor={materialTheme.COLORS.WHITE}
								onChangeText={text => this.handleChange('noteHeader', text)}
								style={[styles.input, this.state.active.noteHeader ? styles.inputActive : null]}
								/>
								{this.state.validation.noteHeader != null &&
									<Text color="orange" size={14}>{this.state.validation.noteHeader}</Text>
                                }
                                <Text
								color={theme.COLORS.WHITE}
								size={theme.SIZES.FONT }
								style={{ alignSelf: 'flex-start', lineHeight: theme.SIZES.FONT * 2 }}
								>
									Notunuz:
								</Text>
								<Input
								color="white"
								placeholder="Notunuz"
                                autoCapitalize="none"
                                multiline
								autoCorrect={false}
								bgColor='transparent'
								onBlur={() => this.toggleActive('noteDescription')}
								onFocus={() => this.toggleActive('noteDescription')}
								placeholderTextColor={materialTheme.COLORS.WHITE}
								onChangeText={text => this.handleChange('noteDescription', text)}
								style={[styles.input, this.state.active.noteDescription ? styles.inputActive : null]}
								/>
								{this.state.validation.noteDescription != null &&
									<Text color="orange" size={14}>{this.state.validation.noteDescription}</Text>
								}
							</Block>
							<Block  top row space="evenly" style={{marginTop: 10, marginBottom: 10}}>
								<Block flex left>
									<Button
									shadowless
									color="rgba(244, 67, 54, 0.9)"
									style={{ height: 36, width: (width - 32)*0.4, borderRadius: 20 }}
									onPress={() => this.props.noteFormVisible()}
									>
									VAZGEÇ
									</Button>
								</Block>
								<Block flex right>
									{this.state.isSubmitting ? 
										<ActivityIndicator animating={true} color={Colors.red800} style={{alignSelf:'center'}}/>
									:
										<Button
										shadowless
										color={this.state.noteHeader != '' && !this.state.noteHeader ? materialTheme.COLORS.DEFAULT : materialTheme.COLORS.TEZMAKSAN}
										disabled={this.state.isSubmitting}
										style={{ height: 36, width: (width - 32)*0.4, borderRadius: 20 }}
										onPress={() => this._handleSubmit()}
										>
										KAYDET
										</Button>
									}
								</Block>
								
							</Block>
						</Block>
					</KeyboardAvoidingView>
				</Block>
            </Block>
		);
	}
}

const styles = StyleSheet.create({
    cart: {
		width: width - 32,
		borderWidth: 1,
		borderColor: 'rgba(94, 109, 114, 0.4)',
        borderRadius: 20,
		alignSelf: "center",
		backgroundColor: 'rgba(94, 109, 114, 0.7)'
	},
	input: {
        width: width * 0.8, 
        minHeight: 40,
        height: 'auto',
		borderRadius: 5,
		borderBottomWidth: 1,
		borderColor: materialTheme.COLORS.WHITE,
	},
	inputActive: {
		borderBottomColor: "white",
	},
});
import React, { Component } from 'react';
import { StyleSheet, Dimensions, ScrollView, TouchableWithoutFeedback, SafeAreaView, Alert } from 'react-native';
import { Block, Text, theme, Icon, Input, Button } from 'galio-framework';
import { materialTheme } from '../../constants/';
import LinearGradient from 'react-native-linear-gradient';
import NavigationService from '../../NavigationService';
import { FAB, Portal, Provider, Card, Divider, Paragraph, ActivityIndicator, Colors } from 'react-native-paper';

import HttpService from '../../utils/HttpService';
import { HeaderHeight } from "../../constants/utils";
import NotificationIcon from '../../components/NotificationIcon';

import Modal from 'react-native-modal';
import { HubConnectionBuilder, LogLevel } from '@aspnet/signalr';
import { SwipeListView } from 'react-native-swipe-list-view';

const { width } = Dimensions.get('screen');
const hubConnection= new HubConnectionBuilder()
    .withUrl("http://apiselect.kapasitematik.com/signalrHub")
    .build()

// pages
import AddForm from './AddForm';
import AddNewNote from './AddNewNote';
import Loading from '../Loading';

export default class Home extends Component {
    constructor(props){
        super(props);
        this.state = {
          item: 7,
          isLoading: true,
          listLoading: true,
          categories: [],
          filteredCategories: [],
          search: '',
          active: false,
          formModalVisible: false,
          noteFormVisible: false,
          activePieceName: null,
          activePieceId: null,
          open: false,
          uniqueValue: 1
        } 
        this.formModalActive = this._formModalActive.bind(this)
        this.retrieveCategories = this._renderCategories.bind(this)
        this.noteModalActive = this._noteModalActive.bind(this)
    }

    static navigationOptions = ({ navigation }) => {
        return {
          title: navigation.getParam('title')+" Takımları",
        };
    };

    componentDidMount() {
        if(this.state.categories.length == 0)
        {
            this._retrieveCategories();
        }
    }

    _endHubConnection = async () => {
        hubConnection.stop().then(
            console.log("Connection stopped.")
        ).catch(e => {
            console.log(e)
        })
    }

    _hubConnection = async () => {
        hubConnection
            .start()
            .then(() => {
                console.log('Connection started!')
                hubConnection.on('ParcaSignalr', message => {
                    //console.log(message)
                    let data = []
                    message.map((items) => {
                        data.push({"key": `${items.pieceId}`, "adet": items.adet, "iyi": items.iyi, "kotu": items.kotu, "normal": items.normal, "pieceName": items.pieceName})
                    })
                    this.setState({categories:data})
                    //this._retrieveCategories();
                })

                hubConnection.onclose(()=>{
                    this.setState({uniqueValue:this.state.uniqueValue + 1})
                })
            })
            .catch(err => {
                console.log('Error while establishing connection: ', err)
                setTimeout(() => this._hubConnection(), 5000);
            });
    }

    _retrieveCategories = async () => {
        const machineID = this.props.navigation.getParam('machineID');
        const result = await HttpService.getCategories(machineID);
        if(!result || result == false) {
            this.setState({
                categories: [],
                filteredCategories: [],
                isLoading: false
            })
        }else if(result==[]){
            this.setState({
                categories: [],
                filteredCategories: [],
                isLoading: false
            })
        }else{
            let data = []
            result.map((items) => {
                data.push({"key": `${items.pieceId}`, "adet": items.adet, "iyi": items.iyi, "kotu": items.kotu, "normal": items.normal, "pieceName": items.pieceName})
            })
            this.setState({
                isLoading: false,
                categories: data,
                filteredCategories: [],
                item: 7,
            })
            await this._endHubConnection()
            this._hubConnection()
        }
    }

    _onStateChange = ({ open }) => this.setState({ open });

    _handleSearchChange = (search) => {
        const filteredCategories = this.state.categories.filter(item => search.toLowerCase() && item.pieceName.toLowerCase().includes(search.toLowerCase()));
        this.setState({ filteredCategories, search });
    }

    _formModalActive = () => {
        this.setState({formModalVisible: !this.state.formModalVisible})
    }

    _noteModalActive = (activePieceName, activePieceId) => {
        this.setState({noteFormVisible: !this.state.noteFormVisible, activePieceName: activePieceName, activePieceId:activePieceId})
    }

    _increaseList = () => {
        this.setState({listLoading: true})
        const dataLength = this.state.categories.length;
        const item = this.state.item;
        if(this.state.item < dataLength)
        {
            this.setState({item: item + 2, listLoading: false})
            return false
        }else{
            this.setState({listLoading: false})
        }
    }

    _deleteCategory = async (id) => {
        try {
			let response = await HttpService.deleteCategory(id);
			if(!response)
			{
				alert("Takım silinirken hata oluştu.")
				return false
			}else{
				this._retrieveCategories();
			}
		}catch (e) {
			console.log("Couldn't delete category.");
		}
    }

    _renderSearch = () => {
        const { search } = this.state;
        const iconSearch = (search ?
          <TouchableWithoutFeedback onPress={() => this.setState({ search: '' })}>
            <Icon size={16} color={theme.COLORS.MUTED} name="page-remove" family="foundation" />
          </TouchableWithoutFeedback> :
          <Icon size={16} color={theme.COLORS.MUTED} name="magnifying-glass" family="entypo" />
        )
        return (
            <Block center>
                <Input
                    right
                    color="black"
                    autoFocus={false}
                    autoCorrect={false}
                    autoCapitalize="none"
                    iconContent={iconSearch}
                    defaultValue={search}
                    style={[styles.search]}
                    placeholderTextColor={materialTheme.COLORS.BLACK}
                    placeholder="Takımlar içerisinde arama yapın."
                    onFocus={() => this.setState({ active: true })}
                    onBlur={() => this.setState({ active: false })}
                    onChangeText={this._handleSearchChange}
                />
            </Block>
        )
    }

    _renderCategories() {
        if(this.state.filteredCategories.length != 0){
            return (
                <SwipeListView
                showsVerticalScrollIndicator={false}
                //disableRightSwipe={true}
                data={this.state.filteredCategories}
                renderItem={ (data, rowMap) => (
                    <TouchableWithoutFeedback
                    key={data.item.key}
                    onPress={() => NavigationService.navigate('Tools', {
                        title: data.item.pieceName,
                        pieceId: data.item.key
                    })}
                    >
                        <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 0.25, y: 1.1 }}
                        locations={[0.2, 1]}
                        colors={['#addeff', '#98c3e0']}
                        style={styles.category}
                        >
                            <Block key={data.item.key}>
                                <Block style={styles.categoryTitle}>
                                    <Block row>
                                        <Block flex row left>
                                            <Icon name="angle-double-right" family="font-awesome" size={20} color="black" style={{ marginRight: 5}}/>
                                            <Text style={{fontWeight:'bold', fontSize:15}} color="black">{data.item.pieceName}</Text>
                                        </Block>
                                        <Block flex right>
                                            <Block row>
                                                <Block>
                                                    <Text color="black" style={{fontWeight:'bold'}}>Toplam Parça</Text>
                                                </Block>
                                                <Block>
                                                    <Text color="#3d3d3d" center style={{fontWeight:'bold', marginLeft:5}}>{data.item.adet}</Text>
                                                </Block>
                                            </Block>
                                        </Block>
                                    </Block>
                                    <Divider style={{backgroundColor:'black', marginVertical:5}}/>
                                    <Block row style={{marginTop: 5, marginBottom: 5}}>
                                        <Block flex left style={{backgroundColor:'rgba(76, 175, 80, 0.8)', borderRadius: 5, marginRight: 5, height: 50, justifyContent:'center'}}>
                                            <Block center >
                                                <Text color="white">İyi Parça</Text>
                                                <Text color="white">{data.item.iyi}</Text>
                                            </Block>
                                        </Block>
                                        <Block flex center style={{backgroundColor:'rgba(255, 152, 0, 0.8)', borderRadius: 5, marginRight: 2.5, marginLeft: 2.5, height: 50, justifyContent:'center'}}>
                                            <Text color="white">Normal Parça</Text>
                                            <Text color="white">{data.item.normal}</Text>
                                        </Block>
                                        <Block flex right style={{backgroundColor:'rgba(244, 67, 54, 0.9)', borderRadius: 5, marginLeft: 5, height: 50, justifyContent:'center'}}>
                                            <Block center>
                                                <Text color="white">Kritik Parça</Text>
                                                <Text center color="white">{data.item.kotu}</Text>
                                            </Block>
                                        </Block>
                                    </Block>
                                </Block>
                            </Block>
                        </LinearGradient>
                    </TouchableWithoutFeedback>
                )}
                renderHiddenItem={ (data, rowMap) => (
                    <Block flex row style={[styles.hidden, {justifyContent:'space-between'}]}>
                        <TouchableWithoutFeedback
                            onPress={()=> this._noteModalActive(data.item.pieceName,data.item.key)}
                        >
                            <Block left style={{justifyContent:'center', alignItems:'center', width: 100, borderRadius:15, backgroundColor:'rgba(0, 80, 255, 0.8)'}}>
                                <Icon size={25} color="#FFF" style={{ marginRight: 25 }} name="sticky-note" family="font-awesome" />
                                <Text color="white" style={{ marginRight: 25 }}>Not Ekle</Text>
                            </Block>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={()=> Alert.alert(   
                                'Takım Sil'
                                ,'Takımı silmek istediğinizden emin misiniz?'
                                ,[
                                {text: 'Evet', onPress: () => this._deleteCategory(data.item.key) },
                                {text: 'Hayır'}
                                ]
                            )}
                        >
                            <Block right style={{justifyContent:'center', alignItems:'center', width: 100, borderRadius: 15, backgroundColor:'rgba(255, 0, 0, 0.8)'}}>
                                <Icon size={25} color="#FFF" style={{ marginLeft: 25 }} name="trash" family="font-awesome" />
                                <Text color="white" style={{ marginLeft: 25 }}>Sil</Text>
                            </Block>
                        </TouchableWithoutFeedback>
                    </Block>
                )}
                leftOpenValue={75}
                rightOpenValue={-75}
                />
            );
        }else if(this.state.categories.length != 0 && this.state.search == '') {
            return (
                <SwipeListView
                showsVerticalScrollIndicator={false}
                //disableRightSwipe={true}
                ListFooterComponent={this.state.item < this.state.categories.length && this.state.filteredCategories.length == 0 ? 
                    <Block center>
                        <Button onPress={this._increaseList} color="rgba(256, 256, 256, 0.3)" style={{ width: 100, height: 30, borderRadius: 15 }}>  Daha Fazla  </Button>
                    </Block>
                    :
                    <Block />
                }
                data={this.state.categories.slice(0,this.state.item)}
                renderItem={ (data, rowMap) => (
                    <TouchableWithoutFeedback
                    key={data.item.key}
                    onPress={() => NavigationService.navigate('Tools', {
                        title: data.item.pieceName,
                        pieceId: data.item.key
                    })}
                    >
                        <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 0.25, y: 1.1 }}
                        locations={[0.2, 1]}
                        colors={['#addeff', '#98c3e0']}
                        style={styles.category}
                        >
                            <Block key={data.item.key}>
                                <Block style={styles.categoryTitle}>
                                    <Block row>
                                        <Block flex row left>
                                            <Icon name="angle-double-right" family="font-awesome" size={20} color="black" style={{ marginRight: 5}}/>
                                            <Text style={{fontWeight:'bold', fontSize:15}} color="black">{data.item.pieceName}</Text>
                                        </Block>
                                        <Block flex right>
                                            <Block row>
                                                <Block>
                                                    <Text color="black" style={{fontWeight:'bold'}}>Toplam Parça</Text>
                                                </Block>
                                                <Block>
                                                    <Text color="#3d3d3d" center style={{fontWeight:'bold', marginLeft:5}}>{data.item.adet}</Text>
                                                </Block>
                                            </Block>
                                        </Block>
                                    </Block>
                                    <Divider style={{backgroundColor:'black', marginVertical:5}}/>
                                    <Block row style={{marginTop: 5, marginBottom: 5}}>
                                        <Block flex left style={{backgroundColor:'rgba(76, 175, 80, 0.8)', borderRadius: 5, marginRight: 5, height: 50, justifyContent:'center'}}>
                                            <Block center >
                                                <Text color="white">İyi Parça</Text>
                                                <Text color="white">{data.item.iyi}</Text>
                                            </Block>
                                        </Block>
                                        <Block flex center style={{backgroundColor:'rgba(255, 152, 0, 0.8)', borderRadius: 5, marginRight: 2.5, marginLeft: 2.5, height: 50, justifyContent:'center'}}>
                                            <Text color="white">Normal Parça</Text>
                                            <Text color="white">{data.item.normal}</Text>
                                        </Block>
                                        <Block flex right style={{backgroundColor:'rgba(244, 67, 54, 0.9)', borderRadius: 5, marginLeft: 5, height: 50, justifyContent:'center'}}>
                                            <Block center>
                                                <Text color="white">Kritik Parça</Text>
                                                <Text center color="white">{data.item.kotu}</Text>
                                            </Block>
                                        </Block>
                                    </Block>
                                </Block>
                            </Block>
                        </LinearGradient>
                    </TouchableWithoutFeedback>
                )}
                renderHiddenItem={ (data, rowMap) => (
                    <Block flex row style={[styles.hidden, {justifyContent:'space-between'}]}>
                        <TouchableWithoutFeedback
                            onPress={()=> this._noteModalActive(data.item.pieceName,data.item.key)}
                        >
                            <Block left style={{justifyContent:'center', alignItems:'center', width: 100, borderRadius:15, backgroundColor:'rgba(0, 80, 255, 0.8)'}}>
                                <Icon size={25} color="#FFF" style={{ marginRight: 25 }} name="sticky-note" family="font-awesome" />
                                <Text color="white" style={{ marginRight: 25 }}>Not Ekle</Text>
                            </Block>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={()=> Alert.alert(   
                                'Takım Sil'
                                ,'Takımı silmek istediğinizden emin misiniz?'
                                ,[
                                {text: 'Evet', onPress: () => this._deleteCategory(data.item.key) },
                                {text: 'Hayır'}
                                ]
                            )}
                        >
                            <Block right style={{justifyContent:'center', alignItems:'center', width: 100, borderRadius: 15, backgroundColor:'rgba(255, 0, 0, 0.8)'}}>
                                <Icon size={25} color="#FFF" style={{ marginLeft: 25 }} name="trash" family="font-awesome" />
                                <Text color="white" style={{ marginLeft: 25 }}>Sil</Text>
                            </Block>
                        </TouchableWithoutFeedback>
                    </Block>
                )}
                leftOpenValue={75}
                rightOpenValue={-75}
                />
            );
        }else {
            return (
                <Block style={styles.emty}>
                    <Icon name="info-circle" family="font-awesome" size={50} color="white" style={{ marginVertical: 5, marginTop: theme.SIZES.BASE * 10}}/>
                    <Text style={{color:'white'}}>Herhangi bir takım bulunamadı.</Text>
                </Block>
            )
        }
    }

    render() {
        const { open } = this.state;

        if (this.state.isLoading) {
            return (
                <Loading />
            )
        }
        return (
            <LinearGradient
				start={{ x: 0, y: 0 }}
				end={{ x: 0.25, y: 1.1 }}
				locations={[0.2, 1]}
				colors={['#3ca4d5', '#003249']}
				style={[styles.home, {flex: 1, paddingTop: theme.SIZES.BASE * 5}]}
            >
                <Block row>
                    {this._renderSearch()}
                    <Block style={{justifyContent:'center', alignItems:'center'}}>
                        <NotificationIcon />
                    </Block>
                </Block>
                <Block style={styles.categoryList}>
                    {this._renderCategories()}
                </Block>
                <Modal isVisible={this.state.formModalVisible}>
                    <AddForm formModalActive = {this._formModalActive} retrieveCategories = {this._retrieveCategories} machineId = {this.props.navigation.getParam('machineID')}/>
                </Modal>
                <Modal isVisible={this.state.noteFormVisible}>
                    <AddNewNote noteFormVisible = {this._noteModalActive} retrieveCategories = {this._retrieveCategories} activePieceName={this.state.activePieceName} activePieceId={this.state.activePieceId}/>
                </Modal>
                <Provider>
                    <Portal>
                        <FAB.Group
                            open={open}
                            icon={open ? 'close' : 'pencil'}
                            actions={[
                                { icon: 'help', label:'Yardım', onPress: () => NavigationService.navigate('Help') },
                                { icon: 'plus', label:'Takım Ekle', onPress: () => this._formModalActive() },
                                { icon: 'file', label:'Rapor', onPress: () => NavigationService.navigate('Report') },
                            ]}
                            onStateChange={this._onStateChange}
                            onPress={() => {
                            if (open) {
                                // do something if the speed dial is open
                            }
                            }}
                        />
                    </Portal>
                </Provider>
            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    home: {
        marginTop: Platform.OS === 'android' ? -theme.SIZES.BASE*2 : 0,
    },
    categoryList: {
      justifyContent: 'center',
      paddingTop: theme.SIZES.BASE * 0.1,
      paddingBottom: 75
    },
    category: {
      backgroundColor: 'transparent',
      marginHorizontal: theme.SIZES.BASE,
      marginVertical: theme.SIZES.BASE / 2,
      borderRadius: 15,
      borderWidth: 0,
    },
    categoryTitle: {
      paddingHorizontal: theme.SIZES.BASE,
      marginVertical: theme.SIZES.BASE / 2,
    },
    emty: {
        paddingHorizontal: theme.SIZES.BASE,
        backgroundColor: 'rgba(0, 0, 0, 0.01)',
        alignItems: 'center'
    },
    search: {
        height: 40,
        width: width - 75,
        marginLeft: theme.SIZES.BASE ,
        marginHorizontal: theme.SIZES.BASE / 2,
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        borderWidth: 0,
        borderRadius: 15,
    },
    shadowSearch: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 3 },
        shadowRadius: 4,
        shadowOpacity: 0.1,
        elevation: 2,
    },
    hidden: {
        flex: 1,
        backgroundColor: 'transparent',
        marginHorizontal: theme.SIZES.BASE,
        marginVertical: theme.SIZES.BASE / 2,
        borderRadius: 15,
        borderWidth: 0,
    },
  });
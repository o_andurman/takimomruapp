import React, { Component } from 'react';

import { StyleSheet, Dimensions, KeyboardAvoidingView, Alert } from "react-native";
import { Block, Text, theme, Input, Button, Icon } from 'galio-framework';
import { materialTheme } from '../../constants/';
import HttpService from '../../utils/HttpService';
import { ActivityIndicator, Colors, Divider } from 'react-native-paper';

const { width } = Dimensions.get('screen');

export default class AddForm extends Component {
	constructor(props){
		super(props);
		this.state = {
			pieceName: null,
			isSubmitting: false,
			active: {
			  pieceName: false,
			},
			validation: {
				pieceName: null,
			}
		}
	}
	
	_handleSubmit = async () => {
		try {
			this.setSubmitting();
			let response = await HttpService.addCategory(this.state.pieceName, this.props.machineId);
			if(!response)
			{
				alert("Takım eklenirken hata oluştu.")
				this.setSubmitting();
				return false
			}else{
				Alert.alert(   
					'Tebrikler'
					,'Takımınız başarıyla eklendi.'
					,[
						{text: 'Tamam', onPress: () => {
							this.props.retrieveCategories();
							this.props.formModalActive();
							} 
						},
					]
				)
			}
		}catch (e) {
			console.log("Couldn't add category.");
		}
	};

	setSubmitting = () => {
		this.setState({isSubmitting: !this.state.isSubmitting})
	}

	handleChange = (name, value) => {
		this.setState({ [name]: value });
		const { validation } = this.state;
		if(name=="pieceName")
		{
			//let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			let reg = /^[a-zA-Z0-9 ğçşüöıĞÇŞÜÖİ.-]+$/;
			if(value == '')
			{
				validation["pieceName"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["pieceName"] = "*Bu alan sadece a-z, A-Z veya 0-9 karakterlerini içermelidir.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["pieceName"] = null;
				this.setState({ validation })
			}
		}
	}

	toggleActive = (name) => {
		const { active } = this.state;
		active[name] = !active[name];

		this.setState({ active });
	}

	render() {
		return (
			<Block center style={[styles.cart]}>
				<Block middle>
					<KeyboardAvoidingView behavior="padding" enabled>
						<Block row middle style={{ paddingVertical: theme.SIZES.BASE * 0.625}}>
							<Icon name="angle-double-right" family="font-awesome" size={20} color="white" style={{marginRight:5}}/>
							<Text center color="white" size={theme.SIZES.FONT * 1.5} >
								Takım Ekle
							</Text>
						</Block>
						<Divider style={{backgroundColor:'white'}}/>
						<Block>
							<Block middle>
								<Text
								color={theme.COLORS.WHITE}
								size={theme.SIZES.FONT }
								style={{ alignSelf: 'flex-start', lineHeight: theme.SIZES.FONT * 2 }}
								>
									Takım Adı:
								</Text>
								<Input
								color="white"
								placeholder="Takım Adı Giriniz"
								autoCapitalize="none"
								autoCorrect={false}
								bgColor='transparent'
								onBlur={() => this.toggleActive('pieceName')}
								onFocus={() => this.toggleActive('pieceName')}
								placeholderTextColor={materialTheme.COLORS.WHITE}
								onChangeText={text => this.handleChange('pieceName', text)}
								style={[styles.input, this.state.active.pieceName ? styles.inputActive : null]}
								/>
								{this.state.validation.pieceName != null &&
									<Text color="white" size={14}>{this.state.validation.pieceName}</Text>
								}
							</Block>
							<Block top row space="evenly" style={{marginTop: 10, marginBottom: 10}}>
								<Block flex left>
									<Button
									shadowless
									color="rgba(244, 67, 54, 0.9)"
									style={{ height: 36, width: (width - 32)*0.4, borderRadius: 20 }}
									onPress={() => this.props.formModalActive()}
									>
									VAZGEÇ
									</Button>
								</Block>
								<Block flex right>
									{this.state.isSubmitting ? 
										<ActivityIndicator animating={true} color={Colors.red800} style={{alignSelf:'center'}}/>
									:
										<Button
										shadowless
										color={this.state.pieceName != '' && !this.state.pieceName ? materialTheme.COLORS.DEFAULT : materialTheme.COLORS.TEZMAKSAN}
										disabled={this.state.isSubmitting}
										style={{ height: 36, width: (width - 32)*0.4, borderRadius: 20 }}
										onPress={() => this._handleSubmit()}
										>
										KAYDET
										</Button>
									}
								</Block>
								
							</Block>
						</Block>
					</KeyboardAvoidingView>
				</Block>
            </Block>
		);
	}
}

const styles = StyleSheet.create({
    cart: {
		width: width - 32,
		marginHorizontal: theme.SIZES.BASE,
		borderWidth: 1,
		borderColor: 'rgba(94, 109, 114, 0.4)',
        borderRadius: 20,
		backgroundColor: 'rgba(94, 109, 114, 0.7)'
	},
	input: {
		width: width * 0.8, 
		borderRadius: 5,
		borderBottomWidth: 1,
		borderColor: materialTheme.COLORS.WHITE,
	},
	inputActive: {
		borderBottomColor: "white",
	},
});
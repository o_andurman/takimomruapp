import React, {Component} from 'react';
import { StyleSheet, Dimensions, KeyboardAvoidingView, Alert, Platform, Image, ScrollView } from 'react-native';
import { Block, Button, Input, Text, theme } from 'galio-framework';
import NavigationService from '../../NavigationService';

import { materialTheme } from '../../constants/';
import { HeaderHeight } from "../../constants/utils";
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import { ActivityIndicator, Colors } from 'react-native-paper';

import { inject } from 'mobx-react';
import HttpService from '../../utils/HttpService';

const { width } = Dimensions.get('window');

@inject('AuthStore')
export default class Signin extends Component {
	constructor(props){
		super(props);
		this.state = {
			isSubmitting: false,
			company: null,
			name: null,
			surname: null,
			email: null,
			username: null,
			password: null,
			image: null,
			active: {
				company: false,
				name: false,
				surname: false,
				email: false,
				username: false,
				password: false,
			},
			validation: {
				company: null,
				name: null,
				surname: null,
				email: null,
				username: null,
				password: null
			}
		}
	}

	handleChange = (name, value) => {
		this.setState({ [name]: value });
		const { validation } = this.state;
		if(name=="company")
		{
			//let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			let reg = /^[a-zA-Z0-9 ğçşüöıĞÇŞÜÖİ.-]+$/;
			if(value == '')
			{
				validation["company"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["company"] = "Firma adı alanı sadece a-z, A-Z veya 0-9 karakterlerini içermelidir.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["company"] = null;
				this.setState({ validation })
			}
		}
		if(name=="name")
		{
			//let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			let reg = /^[a-zA-Z0-9 ğçşüöıĞÇŞÜÖİ]+$/;
			if(value == '')
			{
				validation["name"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["name"] = "Kullanıcı adınız sadece a-z, A-Z veya 0-9 karakterlerini içermelidir.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["name"] = null;
				this.setState({ validation })
			}
		}
		if(name=="surname")
		{
			//let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			let reg = /^[a-zA-Z0-9 ğçşüöıĞÇŞÜÖİ]+$/;
			if(value == '')
			{
				validation["surname"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["surname"] = "Kullanıcı adınız sadece a-z, A-Z veya 0-9 karakterlerini içermelidir.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["surname"] = null;
				this.setState({ validation })
			}
		}
		if(name=="email")
		{
			let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			//let reg = /^[a-zA-Z0-9]+$/;
			if(value == '')
			{
				validation["email"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["email"] = "Lütfen geçerli bir e-posta adresi giriniz.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["email"] = null;
				this.setState({ validation })
			}
		}
		if(name=="username")
		{
			//let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			let reg = /^[a-zA-Z0-9]+$/;
			if(value == '')
			{
				validation["username"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["username"] = "Kullanıcı adınız sadece a-z, A-Z veya 0-9 karakterlerini içermelidir.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["username"] = null;
				this.setState({ validation })
			}
		}
		/*if(name=="password")
		{
			let reg = /^[a-zA-Z0-9]+$/;
			if (reg.test(value) === false) {
				validation["password"] = "Lütfen geçerli bir e-posta adresi giriniz.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["password"] = null;
				this.setState({ validation })
			}
		}*/
	}

	handleSubmit = async () => {
		try {
			this.setSubmitting();
			let response = await HttpService.addUser(this.state.name, this.state.surname, this.state.password, this.state.company, this.state.email, this.state.username, this.state.image);
			if(!response)
			{
				this.setSubmitting();
				alert("Lütfen girdiğiniz bilgileri kontrol ediniz.");
				return false
			}else{
				NavigationService.navigate('SignIn');
			}
		}catch (e) {
			console.log(e);
		}
	}

	setSubmitting = () => {
		this.setState({isSubmitting: !this.state.isSubmitting})
	}

	handleChoosePhoto = () => {
		const options = {
			title: 'Fotoğraf Seç',
			takePhotoButtonTitle: 'Fotoğraf Çek',
			chooseFromLibraryButtonTitle: 'Galeriden Seç',
			cancelButtonTitle: 'Vazgeç',
			storageOptions: {
			  skipBackup: true,
			  path: 'ToolLife',
			},
		};
		ImagePicker.showImagePicker(options, (response) => {
		   
			if (response.didCancel) {
			  console.log('User cancelled image picker');
			} else if (response.error) {
			  console.log('ImagePicker Error: ', response.error);
			} else if (response.customButton) {
			  console.log('User tapped custom button: ', response.customButton);
			} else {
			  	const source = { uri: response.uri };
			  	ImageResizer.createResizedImage(source.uri, 512, 512, 'JPEG', 100)
				.then(response => {
					// response.uri is the URI of the new image that can now be displayed, uploaded...
					// response.path is the path of the new image
					// response.name is the name of the new image with the extension
					// response.size is the size of the new image
					this.setState({
						image: response,
					});
				})
				.catch(err => {
					alert("Fotoğraf eklenirken sorun oluştu.")
					console.log("Couldn't compress image.")
				});
			  	// You can also display the image using data:
			 	// const source = { uri: 'data:image/jpeg;base64,' + response.data };
			}
		  });
	}

	toggleActive = (name) => {
		const { active } = this.state;
		active[name] = !active[name];

		this.setState({ active });
	}

	render() {
		const { navigation } = this.props;
		return (
			<LinearGradient
				start={{ x: 0, y: 0 }}
				end={{ x: 0.25, y: 1.1 }}
				locations={[0.2, 1]}
				colors={['#3ca4d5', '#003249']}
				style={[styles.signin, {flex: 1, paddingTop: theme.SIZES.BASE * 4}]}>
			<Block middle>
					<ScrollView showsVerticalScrollIndicator={false}>
						<Block middle style={{ paddingVertical: theme.SIZES.BASE * 2.625}}>
							<Image
								style={styles.image}
								source={require('../../images/tezmaksan-disi-logo.png')}
							/>
							<Text style={{fontSize: 20, marginTop: 20, color:'white'}}>Kayıt Ol</Text>
						</Block>
						<Block flex>
							<Block center>
								<Input
								borderless
								color="white"
								placeholder="Firma Adı"
								autoCapitalize="none"
								autoCorrect={ false }
								bgColor='transparent'
								onBlur={() => this.toggleActive('company')}
								onFocus={() => this.toggleActive('company')}
								placeholderTextColor={materialTheme.COLORS.PLACEHOLDER}
								onChangeText={text => this.handleChange('company', text)}
								style={[styles.input, this.state.active.company ? styles.inputActive : null]}
								/>
								{this.state.validation.company != null &&
									<Text color="orange" size={14}>{this.state.validation.company}</Text>
								}
								<Input
								borderless
								color="white"
								placeholder="Adınız"
								autoCapitalize="none"
								autoCorrect={ false }
								bgColor='transparent'
								onBlur={() => this.toggleActive('name')}
								onFocus={() => this.toggleActive('name')}
								placeholderTextColor={materialTheme.COLORS.PLACEHOLDER}
								onChangeText={text => this.handleChange('name', text)}
								style={[styles.input, this.state.active.name ? styles.inputActive : null]}
								/>
								{this.state.validation.name != null &&
									<Text color="orange" size={14}>{this.state.validation.name}</Text>
								}
								<Input
								borderless
								color="white"
								placeholder="Soyadınız"
								autoCapitalize="none"
								autoCorrect={ false }
								bgColor='transparent'
								onBlur={() => this.toggleActive('surname')}
								onFocus={() => this.toggleActive('surname')}
								placeholderTextColor={materialTheme.COLORS.PLACEHOLDER}
								onChangeText={text => this.handleChange('surname', text)}
								style={[styles.input, this.state.active.surname ? styles.inputActive : null]}
								/>
								{this.state.validation.surname != null &&
									<Text color="orange" size={14}>{this.state.validation.surname}</Text>
								}
								<Input
								borderless
								color="white"
								placeholder="E-posta Adresiniz"
								autoCapitalize="none"
								autoCorrect={ false }
								bgColor='transparent'
								onBlur={() => this.toggleActive('email')}
								onFocus={() => this.toggleActive('email')}
								placeholderTextColor={materialTheme.COLORS.PLACEHOLDER}
								onChangeText={text => this.handleChange('email', text)}
								style={[styles.input, this.state.active.email ? styles.inputActive : null]}
								/>
								{this.state.validation.email != null &&
									<Text color="orange" size={14}>{this.state.validation.email}</Text>
								}
								<Input
								borderless
								color="white"
								placeholder="Kullanıcı adı"
								autoCapitalize="none"
								autoCorrect={ false }
								bgColor='transparent'
								onBlur={() => this.toggleActive('username')}
								onFocus={() => this.toggleActive('username')}
								placeholderTextColor={materialTheme.COLORS.PLACEHOLDER}
								onChangeText={text => this.handleChange('username', text)}
								style={[styles.input, this.state.active.username ? styles.inputActive : null]}
								/>
								{this.state.validation.username != null &&
									<Text color="orange" size={14}>{this.state.validation.username}</Text>
								}
								<Input
								password
								viewPass
								borderless
								color="white"
								iconColor="white"
								placeholder="Şifre"
								bgColor='transparent'
								onBlur={() => this.toggleActive('password')}
								onFocus={() => this.toggleActive('password')}
								placeholderTextColor={materialTheme.COLORS.PLACEHOLDER}
								onChangeText={text => this.handleChange('password', text)}
								style={[styles.input, this.state.active.password ? styles.inputActive : null]}
								/>
								{this.state.validation.password != null &&
									<Text color="orange" size={14}>{this.state.validation.password}</Text>
								}
								{this.state.image && (
									<Image
										source={{ uri: this.state.image.uri }}
										style={{ width: 100, height: 100, marginBottom: 10 }}
									/>
								)}
								<Button shadowless onPress={this.handleChoosePhoto} style={{backgroundColor:'rgba(255, 255, 255, 0.8)'}}>Fotoğraf Yükle</Button>
							</Block>
							<Block flex style={{ marginTop: 20 }}>
								{this.state.isSubmitting ? 
									<ActivityIndicator animating={true} color="white" style={{alignSelf:'center'}}/>
								:
								<Button
								shadowless
								color={materialTheme.COLORS.TEZMAKSAN}
								style={{ height: 48 }}
								onPress={() => this.handleSubmit()}
								>
								Kayıt Ol
								</Button>
								}
							</Block>
						</Block>
					</ScrollView>
			</Block></LinearGradient>
		);
	}
}

const styles = StyleSheet.create({
	signin: {        
	  marginTop: Platform.OS === 'android' ? -theme.SIZES.BASE*2 : 0,
	},
	social: {
	  width: theme.SIZES.BASE * 3.5,
	  height: theme.SIZES.BASE * 3.5,
	  borderRadius: theme.SIZES.BASE * 1.75,
	  justifyContent: 'center',
	  shadowColor: 'rgba(0, 0, 0, 0.3)',
	  shadowOffset: {
		width: 0,
		height: 4
	  },
	  shadowRadius: 8,
	  shadowOpacity: 1
	},
	input: {
	  width: width * 0.9, 
	  borderRadius: 0,
	  borderBottomWidth: 1,
	  borderBottomColor: materialTheme.COLORS.PLACEHOLDER,
	},
	inputActive: {
	  borderBottomColor: "white",
	},
	image: {
		width: width * 0.9, 
		height: width * 0.3,
		resizeMode: 'contain',
	}
  });
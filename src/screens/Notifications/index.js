import React, { Component } from 'react';
import { StyleSheet, Dimensions, ScrollView, SafeAreaView, TouchableWithoutFeedback } from 'react-native';
import { Block, Text, theme, Icon } from 'galio-framework';
import LinearGradient from 'react-native-linear-gradient';
import { Card, Paragraph, Divider } from 'react-native-paper';

import HttpService from '../../utils/HttpService';
import { HeaderHeight } from "../../constants/utils";
import NotificationIcon from '../../components/NotificationIcon';

import { SwipeListView } from 'react-native-swipe-list-view';

//Pages
import Loading from '../Loading';

const { width } = Dimensions.get('screen');

export default class Home extends Component {
    constructor(props){
        super(props);
        this.state = {
          isLoading: true,
          notifications: [],
        } 
    }

    static navigationOptions = ({ navigation }) => {
        return {
          headerRight: () => <NotificationIcon />,
        };
    };

    componentDidMount() {
        if(this.state.notifications.length == 0)
        {
          this._retrieveCategories()
        }
    }

    _retrieveCategories = async () => {
        const result = await HttpService.getNotifications();
        if(!result || result == false) {
            this.setState({
                isLoading: false
            })
        }else{
            let data = []
            result.map((items) => {
                data.push({"key": `${items.notificationId}`, "notification_Description": items.notification_Description, "count": items.count, "notification_Date": items.notification_Date, "fkuserId": items.fkuserId, "fkuser": items.fkuser})
            })
            this.setState({
                isLoading: false,
                notifications: data
            })
        }
    }
    
    _deleteNotification = async (id) => {
        this.setState({
            isLoading: true
        })
        const result = await HttpService.deleteNotification(id);
        if(!result || result == false) {
            alert("Bildirim silinirken bir hata oluştu.")
            this._retrieveCategories();
        }else{
            this._retrieveCategories();
        }
    }
    
    _fortmatDate(date) {
        var options = { year: 'numeric', month: 'long', day: 'numeric', timeZone:'UTC'};
        return new Date(date).toLocaleTimeString('tr-TR', options);
    }

    _renderCategories() {
        if(this.state.notifications.length != 0 ) {
            return (
                <SwipeListView
                    showsVerticalScrollIndicator={false}
                    disableRightSwipe={true}
                    data={this.state.notifications}
                    renderItem={ (data, rowMap) => (
                        <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 0.25, y: 1.1 }}
                        locations={[0.2, 1]}
                        colors={['#addeff', '#98c3e0']}
                        style={styles.gradient}
                        >
                            <Block style={[styles.category]}>
                                <Block row style={{ marginVertical:5, marginHorizontal:20}}>
                                    <Block>
                                        <Icon size={15} color="rgba(65, 75, 79, 1)" style={{ marginRight:theme.SIZES.BASE / 2 }} name="clock-o" family="font-awesome" />
                                    </Block>
                                    <Block>
                                        <Text color="rgba(65, 75, 79, 1)" style={{fontWeight:'bold'}}>{this._fortmatDate(data.item.notification_Date)}</Text>
                                    </Block>
                                </Block>
                                <Divider style={{backgroundColor:'rgba(65, 75, 79, 1)', marginHorizontal:20}}/>
                                <Block style={{marginHorizontal:20}}>
                                    <Paragraph style={{color:'rgba(65, 75, 79, 1)', textAlign:'justify'}}>{data.item.notification_Description}</Paragraph>
                                </Block>
                            </Block>
                        </LinearGradient>
                    )}
                    renderHiddenItem={ (data, rowMap) => (
                        <TouchableWithoutFeedback
                            onPress={()=> this._deleteNotification(data.item.key)}
                        >
                            <Block row style={styles.hidden}>
                                <Block flex left style={{justifyContent:'center', marginLeft: 25}}>
                                    <Icon size={25} color="#FFF" style={{ marginRight:theme.SIZES.BASE / 2 }} name="trash" family="font-awesome" />
                                </Block>
                                <Block flex right style={{justifyContent:'center', marginRight: 15}}>
                                    <Icon size={25} color="#FFF" style={{ marginRight:theme.SIZES.BASE / 2 }} name="trash" family="font-awesome" />
                                </Block>
                            </Block>
                        </TouchableWithoutFeedback>
                    )}
                    leftOpenValue={75}
                    rightOpenValue={-75}
                />
            );
        }else {
            return (
                <Block style={styles.emty}>
                    <Icon name="info-circle" family="font-awesome" size={50} color="white" style={{ marginVertical: 5, marginTop: theme.SIZES.BASE * 10}}/>
                    <Text style={{color:'white'}}>Herhangi bir bildirim bulunamadı.</Text>
                </Block>
            )
        }
    }

    render() {

        if (this.state.isLoading) {
            return (
                <Loading />
            )
        }
        return (
            <LinearGradient
				start={{ x: 0, y: 0 }}
				end={{ x: 0.25, y: 1.1 }}
				locations={[0.2, 1]}
				colors={['#3ca4d5', '#003249']}
				style={[styles.home, {flex: 1, paddingTop: theme.SIZES.BASE * 5}]}
            >
                {this._renderCategories()}
            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    home: {
        marginTop: Platform.OS === 'android' ? -theme.SIZES.BASE*2 : 0,
    },
    categoryList: {
        justifyContent: 'center',
        paddingTop: theme.SIZES.BASE * 0.1,
        paddingBottom: 100
    },
    category: {
        backgroundColor: 'transparent',
        borderRadius: 15,
        borderWidth: 0,
    },
    gradient: {
        backgroundColor: 'rgba(255, 255, 255, 1)',
        marginHorizontal: theme.SIZES.BASE,
        marginVertical: theme.SIZES.BASE / 4,
        borderRadius: 15,
        borderWidth: 0,
    },
    body: {
        width: width - 64,
        paddingTop: theme.SIZES.BASE * 0.2,
    },
    hidden: {
        flex: 1,
        backgroundColor: 'rgba(255, 0, 0, 0.8)',
        marginHorizontal: theme.SIZES.BASE,
        marginVertical: theme.SIZES.BASE / 4,
        borderRadius: 15,
        borderWidth: 0,
    },
    emty: {
        paddingHorizontal: theme.SIZES.BASE,
        backgroundColor: 'rgba(0, 0, 0, 0.01)',
        alignItems: 'center'
    },
  });
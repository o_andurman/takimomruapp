import React from 'react';
import { StyleSheet, Dimensions, ScrollView, Image, Platform, TouchableWithoutFeedback, Alert } from 'react-native';
import { Block, Text, theme, Icon, Button } from 'galio-framework';
import LinearGradient from 'react-native-linear-gradient';

import { HeaderHeight } from "../../constants/utils";
import HttpService from '../../utils/HttpService';
import { materialTheme } from '../../constants/';

import LogoutButton from '../../components/LogoutButton';
import { Paragraph, Divider } from 'react-native-paper';
import { SwipeListView } from 'react-native-swipe-list-view';

const { width } = Dimensions.get('screen');

// pages
import Loading from '../Loading';
import ProfileSettings from './ProfileSettings';

export default class Profile extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      noteLoading: true,
      imageLoading: true,
      account: [],
      notes: [],
      pieceNotes: [],
      subPieceNotes: [],
      noteType: "piece",
      collapse: {
        Notes: true,
        Settings: true,
      },
    } 

    this._retrieveAccount = this._retrieveAccount.bind(this)
  }
  
  static navigationOptions = ({ navigation }) => {
      return {
        headerRight: () => <LogoutButton />,
      };
  };

  componentDidMount() {
    if(this.state.account.length == 0)
    {
      this._retrieveAccount()
      this._retrieveNotes()
    }
  }

  _retrieveAccount = async () => {
    const result = await HttpService.getAccount();
    if(!result || result == false) {
        this.setState({
            isLoading: true
        })
    }else{
        this.setState({
            isLoading: false,
            account: result,
            collapse:{Notes: true, Settings:true}
        })
    }
  }

  _retrieveNotes = async () => {
    const result = await HttpService.getNote();
    const pieceNotes = [];
    const subPieceNotes = [];
    if(!result || result == false) {
      return false
    }else{
        result.map((items) => {
          if(!items.type){
            pieceNotes.push({"key": `${items.noteId}`, "createdDate": items.createdDate , "fkpieceId": items.fkpieceId , "fksubpieceId": items.fksubpieceId , "fkuser": items.fkuser , "fkuserId": items.fkuserId , "noteDescription": items.noteDescription , "noteHeader": items.noteHeader , "noteId": items.noteId , "pieceName": items.pieceName , "subPieceName": items.subPieceName , "type": items.type})
          }else{
            subPieceNotes.push({"key": `${items.noteId}`, "createdDate": items.createdDate , "fkpieceId": items.fkpieceId , "fksubpieceId": items.fksubpieceId , "fkuser": items.fkuser , "fkuserId": items.fkuserId , "noteDescription": items.noteDescription , "noteHeader": items.noteHeader , "noteId": items.noteId , "pieceName": items.pieceName , "subPieceName": items.subPieceName , "type": items.type})
          }
        });
        this.setState({
            notes: result,
            pieceNotes: pieceNotes,
            subPieceNotes: subPieceNotes
        })
    }
  }

  _deleteNote = async (id) => {
    try {
      this.setState({notes: [], pieceNotes: [], subPieceNotes: []})
      let response = await HttpService.deleteNote(id);
      console.log(response)
			if(!response)
			{
        console.log(response)
        alert("Not silinirken hata oluştu.")
        this._retrieveNotes();
				return false
			}else{
				this._retrieveNotes();
			}
		}catch (e) {
			console.log("Couldn't delete note.");
		}
  }

  _fortmatDate(date) {
    var options = { year: 'numeric', month: 'long', day: 'numeric', timeZone:'UTC'};
    return new Date(date).toLocaleDateString('tr-TR', options);
  }

  _handleNotetype( type ) {
    this.setState({
      noteType: type
    })
  }

  _renderPieceNotes = () => {
    if(this.state.pieceNotes.length != 0 && this.state.noteType == "piece") {
      return (
        <SwipeListView
        showsVerticalScrollIndicator={false}
        disableRightSwipe={true}
        scrollEnabled={false}
        data={this.state.pieceNotes}
        renderItem={ (data, rowMap) => (
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 0.25, y: 1.1 }}
            locations={[0.2, 1]}
            colors={['#addeff', '#98c3e0']}
            style={styles.gradient}
          >
            <Block style={styles.category} key={data.item.noteId}>
              <Block style={styles.card}>
                  <Block row >
                      <Block>
                          <Icon size={23} color="#000" style={{ marginRight:theme.SIZES.BASE / 2 }} name="angle-double-right" family="font-awesome" />
                      </Block>
                      <Block>
                          <Text color="#000" style={{fontWeight:'bold', fontSize:20}}>{data.item.noteHeader}</Text>
                      </Block>
                      <Block flex right>
                          <Block row>
                            <Block >
                              <Paragraph style={{color:'rgba(0, 0, 0, 0.9)'}}>{data.item.pieceName}</Paragraph>
                            </Block>
                          </Block>
                      </Block>
                  </Block>
                  <Divider style={{backgroundColor:'#000'}}/>
                  <Block style={styles.body}>
                      <Paragraph style={{color:'rgba(0, 0, 0, 0.9)'}}>{data.item.noteDescription}</Paragraph>
                  </Block>
                  <Block row style={{marginTop:5}}>
                    <Block flex right>
                      <Block row>
                        <Block>
                          <Icon size={15} color="#000" style={{ marginRight:theme.SIZES.BASE / 2 }} name="clock-o" family="font-awesome" />
                        </Block>
                        <Block>
                            <Text color="#000" style={{fontWeight:'bold'}}>{this._fortmatDate(data.item.createdDate)}</Text>
                        </Block>
                      </Block>
                    </Block>
                  </Block>
                </Block>
            </Block>
          </LinearGradient>
        )}
        renderHiddenItem={ (data, rowMap) => (
            <TouchableWithoutFeedback
                onPress={()=> Alert.alert(   
                  'Not Sil'
                  ,'Seçilen notu silmek istediğinizden emin misiniz?'
                  ,[
                  {text: 'Evet', onPress: () => this._deleteNote(data.item.noteId) },
                  {text: 'Hayır'}
                  ]
                  )}
            >
                <Block row style={styles.hidden}>
                    <Block flex left style={{justifyContent:'center', marginLeft: 25}}>
                        <Icon size={25} color="#FFF" style={{ marginRight:theme.SIZES.BASE / 2 }} name="trash" family="font-awesome" />
                    </Block>
                    <Block flex right style={{justifyContent:'center', marginRight: 15}}>
                        <Icon size={25} color="#FFF" style={{ marginRight:theme.SIZES.BASE / 2 }} name="trash" family="font-awesome" />
                    </Block>
                </Block>
            </TouchableWithoutFeedback>
        )}
        leftOpenValue={75}
        rightOpenValue={-75}
    />
      );
    }else if(this.state.subPieceNotes.length != 0 && this.state.noteType == "subPiece"){
      return (
      <SwipeListView
        showsVerticalScrollIndicator={false}
        disableRightSwipe={true}
        scrollEnabled={false}
        data={this.state.subPieceNotes}
        renderItem={ (data, rowMap) => (
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 0.25, y: 1.1 }}
            locations={[0.2, 1]}
            colors={['#addeff', '#98c3e0']}
            style={styles.gradient}
          >
            <Block style={styles.category} key={data.item.noteId}>
              <Block style={styles.card}>
                  <Block row >
                      <Block>
                          <Icon size={23} color="#000" style={{ marginRight:theme.SIZES.BASE / 2 }} name="angle-double-right" family="font-awesome" />
                      </Block>
                      <Block>
                          <Text color="#000" style={{fontWeight:'bold', fontSize:20}}>{data.item.noteHeader}</Text>
                      </Block>
                      <Block flex right>
                          <Block row>
                            <Block >
                              <Paragraph style={{color:'rgba(0, 0, 0, 0.9)'}}>{data.item.subPieceName}</Paragraph>
                            </Block>
                          </Block>
                      </Block>
                  </Block>
                  <Divider style={{backgroundColor:'#000'}}/>
                  <Block style={styles.body}>
                      <Paragraph style={{color:'rgba(0, 0, 0, 0.9)'}}>{data.item.noteDescription}</Paragraph>
                  </Block>
                  <Block row style={{marginTop:5}}>
                    <Block flex right>
                      <Block row>
                        <Block>
                          <Icon size={15} color="#000" style={{ marginRight:theme.SIZES.BASE / 2 }} name="clock-o" family="font-awesome" />
                        </Block>
                        <Block>
                            <Text color="#000" style={{fontWeight:'bold'}}>{this._fortmatDate(data.item.createdDate)}</Text>
                        </Block>
                      </Block>
                    </Block>
                  </Block>
                </Block>
            </Block>
          </LinearGradient>
        )}
        renderHiddenItem={ (data, rowMap) => (
            <TouchableWithoutFeedback
                onPress={()=> Alert.alert(   
                  'Not Sil'
                  ,'Seçilen notu silmek istediğinizden emin misiniz?'
                  ,[
                  {text: 'Evet', onPress: () => this._deleteNote(data.item.noteId) },
                  {text: 'Hayır'}
                  ]
                  )}
            >
                <Block row style={styles.hidden}>
                    <Block flex left style={{justifyContent:'center', marginLeft: 25}}>
                        <Icon size={25} color="#FFF" style={{ marginRight:theme.SIZES.BASE / 2 }} name="trash" family="font-awesome" />
                    </Block>
                    <Block flex right style={{justifyContent:'center', marginRight: 15}}>
                        <Icon size={25} color="#FFF" style={{ marginRight:theme.SIZES.BASE / 2 }} name="trash" family="font-awesome" />
                    </Block>
                </Block>
            </TouchableWithoutFeedback>
        )}
        leftOpenValue={75}
        rightOpenValue={-75}
      />
      );
    }else {
      return (
        <Block style={styles.emty}>
            <Icon name="info-circle" family="font-awesome" size={50} color="white" style={{ marginVertical: 5}}/>
            <Text style={{color:'white'}}>Herhangi bir not bulunamadı.</Text>
        </Block>
      )
    }
  }

  render() {
    const { account } = this.state;
    if (this.state.isLoading) {
      return (
          <Loading />
      )
    }
    return (
      <LinearGradient
				start={{ x: 0, y: 0 }}
				end={{ x: 0.25, y: 1.1 }}
				locations={[0.2, 1]}
				colors={['#3ca4d5', '#003249']}
				style={[styles.home, {flex: 1, paddingTop: theme.SIZES.BASE * 5}]}
      >
        <ScrollView showsVerticalScrollIndicator={false}>
          <Block flex style={styles.profile}>
            <Block style={{ marginTop: theme.SIZES.BASE }}>
              <Block style={{elevation: (Platform.OS === 'android') ? 50 : 0, zIndex:2}}>
                <Image
                  style={styles.logo}
                  //test resim url : https://i.pinimg.com/564x/d9/56/9b/d9569bbed4393e2ceb1af7ba64fdf86a.jpg
                  source={{uri: account[0].companyLogo}}
                  onLoad={() => this.setState({imageLoading: false})}
                />
              </Block>
              <Block style={styles.options}>
                <Block center style={styles.header}>
                  <Text size={30} color="white">{account[0].firstName} {account[0].lastName}</Text>
                  <Text size={15} color="rgba(256, 256, 256, 1)">{account[0].company}</Text>
                </Block>
                <Block style={{maxWidth : 235}}>
                  <Block row style={{marginVertical: 10}}>
                    <Block >
                      <Text size={20} color="white">Kullanıcı Adı:</Text>
                    </Block>
                    <Text size={20} color="white"> {account[0].userName}</Text>
                  </Block>
                  <Block row>
                    <Block >
                      <Text size={20} color="white">Mail Adresi:</Text>
                    </Block>
                    <Text size={20} color="white"> {account[0].email}</Text>
                  </Block>
                </Block>
              </Block>
            </Block>
          </Block>
          <Block style={styles.config}>
            <TouchableWithoutFeedback
              onPress={() => this.setState({collapse:{Notes:this.state.collapse.Notes, Settings:!this.state.collapse.Settings}})}
            >
              <Block row space="between" style={{ marginVertical: theme.SIZES.BASE / 2 }}>
                <Block row>
                    <Icon name="cogs" family="font-awesome" size={20} color="white" />
                    <Text size={20} bold color={theme.COLORS.WHITE} style={{ paddingLeft:7 }}>Profili Düzenle</Text>
                </Block>
                <Block row>
                  
                      {this.state.collapse.Settings ? 
                          <Icon name="angle-left" family="font-awesome" size={20} color="white"/>
                          :
                          <Icon name="angle-down" family="font-awesome" size={20} color="white"/>
                      }
                  
                </Block>
              </Block>
            </TouchableWithoutFeedback>
            {this.state.collapse.Settings ? 
              <Block />
              :
              <Block>
                <ProfileSettings retrieveAccount = {this._retrieveAccount}/>
              </Block>
            }
          </Block>
          <Block style={styles.config}>
            <TouchableWithoutFeedback
              onPress={() => this.setState({collapse:{Notes:!this.state.collapse.Notes, Settings:this.state.collapse.Settings}})}
            >
              <Block row space="between" style={{ marginVertical: theme.SIZES.BASE / 2 }}>
                <Block row>
                    <Icon name="clipboard" family="font-awesome" size={20} color="white" />
                    <Text size={20} bold color={theme.COLORS.WHITE} style={{ paddingLeft:7 }}>Notlarım</Text>
                </Block>
                <Block row>
                  
                      {this.state.collapse.Notes ? 
                          <Icon name="angle-left" family="font-awesome" size={20} color="white"/>
                          :
                          <Icon name="angle-down" family="font-awesome" size={20} color="white"/>
                      }
                  
                </Block>
              </Block>
            </TouchableWithoutFeedback>
            {this.state.collapse.Notes ? 
              <Block />
              :
              <Block>
                <Block row>
                  <Button shadowless onPress={() => this.setState({noteType:"piece"})} style={[styles.button, {backgroundColor: this.state.noteType == 'piece' ? materialTheme.COLORS.TEZMAKSAN : materialTheme.COLORS.DEFAULT}]}>Takıma Ait Notlar</Button>
                  <Button shadowless onPress={() => this.setState({noteType:"subPiece"})} style={[styles.button, {backgroundColor: this.state.noteType == 'subPiece' ? materialTheme.COLORS.TEZMAKSAN : materialTheme.COLORS.DEFAULT}]}>Parçaya Ait Notlar</Button>
                </Block>
                {this._renderPieceNotes()}
              </Block>
            }
          </Block>
        </ScrollView>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  profile: {
    marginTop: Platform.OS === 'android' ? -theme.SIZES.BASE : 0,
  },
  options: {
    paddingHorizontal: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: -50,
    paddingBottom: 13,
    borderRadius: 13,
    backgroundColor: 'rgba(256, 256, 256, 0.3)',
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 1,
  },
  logo: {
    width: width / 4,
    height: width / 4,
    resizeMode: 'cover',
    alignSelf: 'center',
    paddingHorizontal: theme.SIZES.BASE,
    backgroundColor: 'white',
    borderRadius: 500,
    borderColor: 'gray',
    borderWidth: 0.3,
    zIndex: 2,
  },
  card: {
    paddingHorizontal: theme.SIZES.BASE,
  },
  header: {
    marginTop: 50,
    textAlign: 'center'
  },
  config: {
    paddingHorizontal: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: theme.SIZES.BASE / 2,
    backgroundColor: 'rgba(256, 256, 256, 0.3)',
    borderRadius: 13,
  },
  emty: {
    backgroundColor: 'rgba(0, 0, 0, 0.01)',
    alignItems: 'center',
    marginBottom: 10
  },
  category: {
    marginVertical: theme.SIZES.BASE / 2,
    borderRadius: 15,
    borderWidth: 0,
  },
  gradient: {
    marginVertical: theme.SIZES.BASE / 2,
    borderRadius: 15,
    borderWidth: 0,
  },
  body: {
    width: width - 64,
    paddingTop: theme.SIZES.BASE * 0.2,
  },
  button: {
    width: (width - 64) / 2

  },
  hidden: {
    flex: 1,
    backgroundColor: 'rgba(255, 0, 0, 0.8)',
    marginVertical: theme.SIZES.BASE / 2,
    borderRadius: 15,
    borderWidth: 0,
  },
});

import React, { Component } from 'react';

import { StyleSheet, Dimensions, KeyboardAvoidingView } from "react-native";
import { Block, Text, theme, Input, Button, Icon } from 'galio-framework';
import { materialTheme } from '../../constants/';
import HttpService from '../../utils/HttpService';
import { ActivityIndicator, Colors, Divider } from 'react-native-paper';

const { width } = Dimensions.get('screen');

export default class ProfileSettings extends Component {
	constructor(props){
		super(props);
		this.state = {
            company: null,
			name: null,
			surname: null,
			email: null,
			pieceName: null,
			isSubmitting: false,
			active: {
				company: false,
				name: false,
				surname: false,
				email: false,
			},
			validation: {
				company: null,
				name: null,
				surname: null,
				email: null,
			}
		}
	}
	
	_handleSubmit = async () => {
		try {
			this.setSubmitting();
			let response = await HttpService.updateAccount(this.state.name, this.state.surname, this.state.company,  this.state.email);
			if(!response)
			{
				alert("Takım eklenirken hata oluştu.")
				this.setSubmitting();
				return false
			}else{
				this.props.retrieveAccount();
				this.setSubmitting();
				alert("Profiliniz başarıyla güncellendi.")
			}
		}catch (e) {
			console.log("Couldn't update settings.");
		}
	};

	setSubmitting = () => {
		this.setState({isSubmitting: !this.state.isSubmitting})
	}

	handleChange = (name, value) => {
		this.setState({ [name]: value });
		const { validation } = this.state;
		if(name=="company")
		{
			//let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			let reg = /^[a-zA-Z0-9 ğçşüöıĞÇŞÜÖİ.-]+$/;
			if(value == '')
			{
				validation["company"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["company"] = "Firma adı alanı sadece a-z, A-Z veya 0-9 karakterlerini içermelidir.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["company"] = null;
				this.setState({ validation })
			}
		}
		if(name=="name")
		{
			//let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			let reg = /^[a-zA-Z ğçşüöıĞÇŞÜÖİ]+$/;
			if(value == '')
			{
				validation["name"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["name"] = "Adınız sadece a-z, A-Z karakterlerini içermelidir.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["name"] = null;
				this.setState({ validation })
			}
		}
		if(name=="surname")
		{
			//let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			let reg = /^[a-zA-Z ğçşüöıĞÇŞÜÖİ]+$/;
			if(value == '')
			{
				validation["surname"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["surname"] = "Soyadınız sadece a-z, A-Z karakterlerini içermelidir.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["surname"] = null;
				this.setState({ validation })
			}
		}
		if(name=="email")
		{
			let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			//let reg = /^[a-zA-Z0-9]+$/;
			if(value == '')
			{
				validation["email"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["email"] = "Lütfen geçerli bir e-posta adresi giriniz.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["email"] = null;
				this.setState({ validation })
			}
		}
	}

	toggleActive = (name) => {
		const { active } = this.state;
		active[name] = !active[name];

		this.setState({ active });
	}

	render() {
		return (
			<Block flex center >
				<Block flex middle>
                    <Divider style={{backgroundColor:'black'}}/>
                    <Block flex>
                        <Block middle>
                            <Text
                            color={theme.COLORS.WHITE}
                            size={theme.SIZES.FONT }
                            style={{ alignSelf: 'flex-start', lineHeight: theme.SIZES.FONT * 2 }}
                            >
                                Adınız:
                            </Text>
                            <Input
                            color="white"
                            placeholder="Adınız"
                            autoCapitalize="none"
                            autoCorrect={false}
                            bgColor='transparent'
                            onBlur={() => this.toggleActive('name')}
                            onFocus={() => this.toggleActive('name')}
                            placeholderTextColor={theme.COLORS.WHITE}
                            onChangeText={text => this.handleChange('name', text)}
                            style={[this.state.active.name ? styles.inputActive : null, {borderColor:'white'}]}
                            />
                            {this.state.validation.name != null &&
                                <Text color="orange" size={14} >{this.state.validation.name}</Text>
                            }
                        </Block>
                        <Block middle>
                            <Text
                            color={theme.COLORS.WHITE}
                            size={theme.SIZES.FONT }
                            style={{ alignSelf: 'flex-start', lineHeight: theme.SIZES.FONT * 2 }}
                            >
                                Soyadınız:
                            </Text>
                            <Input
                            color="white"
                            placeholder="Soyadınız"
                            autoCapitalize="none"
                            autoCorrect={false}
                            bgColor='transparent'
                            onBlur={() => this.toggleActive('surname')}
                            onFocus={() => this.toggleActive('surname')}
                            placeholderTextColor={theme.COLORS.WHITE}
                            onChangeText={text => this.handleChange('surname', text)}
                            style={[this.state.active.surname ? styles.inputActive : null, {borderColor:'white'}]}
                            />
                            {this.state.validation.surname != null &&
                                <Text color="orange" size={14}>{this.state.validation.surname}</Text>
                            }
                        </Block>
                        <Block middle>
                            <Text
                            color={theme.COLORS.WHITE}
                            size={theme.SIZES.FONT }
                            style={{ alignSelf: 'flex-start', lineHeight: theme.SIZES.FONT * 2 }}
                            >
                                Firma İsmi:
                            </Text>
                            <Input
                            color="white"
                            placeholder="Firma İsmi"
                            autoCapitalize="none"
                            autoCorrect={false}
                            bgColor='transparent'
                            onBlur={() => this.toggleActive('company')}
                            onFocus={() => this.toggleActive('company')}
                            placeholderTextColor={theme.COLORS.WHITE}
                            onChangeText={text => this.handleChange('company', text)}
                            style={[this.state.active.company ? styles.inputActive : null, {borderColor:'white'}]}
                            />
                            {this.state.validation.company != null &&
                                <Text color="orange" size={14}>{this.state.validation.company}</Text>
                            }
                        </Block>
                        <Block middle>
                            <Text
                            color={theme.COLORS.WHITE}
                            size={theme.SIZES.FONT }
                            style={{ alignSelf: 'flex-start', lineHeight: theme.SIZES.FONT * 2 }}
                            >
                                E-posta Adresi:
                            </Text>
                            <Input
                            color="white"
                            placeholder="E-posta Adresi"
                            autoCapitalize="none"
                            autoCorrect={false}
                            bgColor='transparent'
                            onBlur={() => this.toggleActive('email')}
                            onFocus={() => this.toggleActive('email')}
                            placeholderTextColor={theme.COLORS.WHITE}
                            onChangeText={text => this.handleChange('email', text)}
                            style={[this.state.active.email ? styles.inputActive : null, {borderColor:'white'}]}
                            />
                            {this.state.validation.email != null &&
                                <Text color="orange" size={14}>{this.state.validation.email}</Text>
                            }
                        </Block>
                        <Block flex top row space="evenly" style={{marginVertical: 10}}>
                            <Block flex right>
                                {this.state.isSubmitting ? 
                                    <ActivityIndicator animating={true} color={Colors.white} style={{alignSelf:'center'}}/>
                                :
                                    <Button
                                    shadowless
                                    color={materialTheme.COLORS.TEZMAKSAN}
                                    disabled={this.state.isSubmitting}
                                    style={{ height: 30, width: (width - 32)*0.4, borderRadius: 20 }}
                                    onPress={() => this._handleSubmit()}
                                    >
                                    KAYDET
                                    </Button>
                                }
                            </Block>
                            
                        </Block>
                    </Block>
				</Block>
            </Block>
		);
	}
}

const styles = StyleSheet.create({
});
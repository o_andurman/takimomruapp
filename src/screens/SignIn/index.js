import React, {Component} from 'react';
import { StyleSheet, Dimensions, ScrollView, Alert, Platform, Image, TouchableWithoutFeedback, Linking } from 'react-native';
import { Block, Button, Input, Text, theme } from 'galio-framework';

import { materialTheme } from '../../constants/';
import { HeaderHeight } from "../../constants/utils";
import LinearGradient from 'react-native-linear-gradient';
import { ActivityIndicator, Colors } from 'react-native-paper';

import { inject } from 'mobx-react';
import HttpService from '../../utils/HttpService';
import NavigationService from '../../NavigationService';

const { width } = Dimensions.get('window');

@inject('AuthStore')
export default class Signin extends Component {
	constructor(props){
		super(props);
		this.state = {
			isSubmitting: false,
			email: null,
			password: null,
			active: {
			  email: false,
			  password: false,
			},
			validation: {
				email: null,
				password: null
			}
		}
	}

	handleChange = (name, value) => {
		this.setState({ [name]: value });
		const { validation } = this.state;
		if(name=="email")
		{
			//let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			let reg = /^[a-zA-Z0-9]+$/;
			if(value == '')
			{
				validation["email"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["email"] = "Kullanıcı adınız sadece a-z, A-Z veya 0-9 karakterlerini içermelidir.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["email"] = null;
				this.setState({ validation })
			}
		}
		/*if(name=="password")
		{
			let reg = /^[a-zA-Z0-9]+$/;
			if (reg.test(value) === false) {
				validation["password"] = "Lütfen geçerli bir e-posta adresi giriniz.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["password"] = null;
				this.setState({ validation })
			}
		}*/
	}

	handleSubmit = async () => {
		try {
			this.setSubmitting();
			let response = await HttpService.auth(this.state.email, this.state.password);
			if(!response.token)
			{
				alert("Kullanıcı adı veya parola hatalı.");
				this.setSubmitting();
				return false
			}else{
				if(this.state.email=="pimsun")
				{
					await this.props.AuthStore.setUser(this.state.email);
					await this.props.AuthStore.saveToken(response.token, this.state.email);
				}else{
					await this.props.AuthStore.saveToken(response.token, this.state.email);
				}
			}
		}catch (e) {
			console.log("Couldn't authenticate.");
			this.setSubmitting();
		}
	}

	setSubmitting = () => {
		this.setState({isSubmitting: !this.state.isSubmitting})
	}

	toggleActive = (name) => {
		const { active } = this.state;
		active[name] = !active[name];

		this.setState({ active });
	}

	render() {
		const { navigation } = this.props;
    	const { email, password } = this.state;
		return (
			<LinearGradient
				start={{ x: 0, y: 0 }}
				end={{ x: 0.25, y: 1.1 }}
				locations={[0.2, 1]}
				colors={['#3ca4d5', '#003249']}
				style={[styles.signin, {flex: 1, paddingTop: theme.SIZES.BASE * 4}]}>
			<Block flex middle>
				<ScrollView showsVerticalScrollIndicator={false} behavior="padding" enabled>
					<Block middle style={{ paddingVertical: theme.SIZES.BASE * 2.625}}>
						<Image
							style={styles.image}
							source={require('../../images/tmekdisi.png')}
						/>
						<Text style={{fontSize: 15, marginTop: -20, color:'white'}}>Takım Ömrü Yönetimi</Text>
						<Text style={{fontSize: 20, marginTop: 20, color:'white'}}>Giriş</Text>
					</Block>
					<Block flex>
						<Block center>
							<Input
							borderless
							color="white"
							placeholder="Kullanıcı adı"
							type="email-address"
							autoCapitalize="none"
							autoCorrect={ false }
							bgColor='transparent'
							onBlur={() => this.toggleActive('email')}
							onFocus={() => this.toggleActive('email')}
							placeholderTextColor={materialTheme.COLORS.PLACEHOLDER}
							onChangeText={text => this.handleChange('email', text)}
							style={[styles.input, this.state.active.email ? styles.inputActive : null]}
							/>
							{this.state.validation.email != null &&
								<Text color="orange" size={14}>{this.state.validation.email}</Text>
							}
							<Input
							password
							viewPass
							borderless
							color="white"
							iconColor="white"
							placeholder="Şifre"
							bgColor='transparent'
							onBlur={() => this.toggleActive('password')}
							onFocus={() => this.toggleActive('password')}
							placeholderTextColor={materialTheme.COLORS.PLACEHOLDER}
							onChangeText={text => this.handleChange('password', text)}
							style={[styles.input, this.state.active.password ? styles.inputActive : null]}
							/>
							{this.state.validation.password != null &&
								<Text color="orange" size={14}>{this.state.validation.password}</Text>
							}
						</Block>
						<Block flex top style={{ marginTop: 20 }}>
							{this.state.isSubmitting ? 
								<ActivityIndicator animating={true} color="white" style={{alignSelf:'center'}}/>
							:
							<Button
							shadowless
							color={materialTheme.COLORS.TEZMAKSAN}
							disabled={!this.state.email || !this.state.password}
							style={{ height: 48 }}
							onPress={() => this.handleSubmit()}
							>
							Oturum Aç
							</Button>
							}
							<Button color="transparent" shadowless onPress={() => navigation.navigate('Sign Up')}>
							<TouchableWithoutFeedback
							onPress={() => NavigationService.navigate('SignUp')}
							>
								<Text
									center
									color={theme.COLORS.WHITE}
									size={theme.SIZES.FONT * 0.75}
									style={{marginTop:20}}
								>
									{"Hesabınız yok mu? Kayıt Olun"}
								</Text>
							</TouchableWithoutFeedback>
							</Button>
						</Block>
					</Block>
					<Block style={{marginTop: 90}}>
						<Block center>
							<Text style={{fontSize: 15, marginTop: 20, color:'white'}}>Sosyal medyada bizi takip edin!</Text>
						</Block>
						<Block middle style={{marginBottom: theme.SIZES.BASE * 5}}>
							<Block row center space="between" style={{ marginVertical: theme.SIZES.BASE * 1.875 }}>
								<Block flex middle right>
								<Button
									round
									onlyIcon
									iconSize={theme.SIZES.BASE * 1.625}
									icon="facebook"
									iconFamily="font-awesome"
									color={theme.COLORS.FACEBOOK}
									shadowless
									iconColor={theme.COLORS.WHITE}
									style={styles.social}
									onPress={() => Linking.openURL('https://tr-tr.facebook.com/TezmaksanAS/')}
								/>
								</Block>
								<Block flex middle center>
								<Button
									round
									onlyIcon
									iconSize={theme.SIZES.BASE * 1.625}
									icon="twitter"
									iconFamily="font-awesome"
									color={theme.COLORS.TWITTER}
									shadowless
									iconColor={theme.COLORS.WHITE}
									style={styles.social}
									onPress={() => Linking.openURL('https://twitter.com/TezmaksanMakina?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor')}
								/>
								</Block>
								<Block flex middle left>
								<Button
									round
									onlyIcon
									iconSize={theme.SIZES.BASE * 1.625}
									icon="linkedin"
									iconFamily="font-awesome"
									color={materialTheme.COLORS.LINKEDIN}
									shadowless
									iconColor={theme.COLORS.WHITE}
									style={styles.social}
									onPress={() => Linking.openURL('https://ir.linkedin.com/company/tezmaksan')}
								/>
								</Block>
							</Block>
						</Block>
					</Block>
				</ScrollView>
			</Block>
			</LinearGradient>
		);
	}
}

const styles = StyleSheet.create({
	signin: {        
	  marginTop: Platform.OS === 'android' ? -theme.SIZES.BASE*2 : 0,
	},
	social: {
	  width: theme.SIZES.BASE * 3.5,
	  height: theme.SIZES.BASE * 3.5,
	  borderRadius: theme.SIZES.BASE * 1.75,
	  justifyContent: 'center',
	  shadowColor: 'rgba(0, 0, 0, 0.3)',
	  shadowOffset: {
		width: 0,
		height: 4
	  },
	  shadowRadius: 8,
	  shadowOpacity: 1
	},
	input: {
	  width: width * 0.9, 
	  borderRadius: 0,
	  borderBottomWidth: 1,
	  borderBottomColor: materialTheme.COLORS.PLACEHOLDER,
	},
	inputActive: {
	  borderBottomColor: "white",
	},
	image: {
		width: width * 0.5, 
		height: width * 0.2,
		resizeMode: 'contain',
	}
  });
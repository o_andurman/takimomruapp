import React, { Component } from 'react';
import { StyleSheet, Image, Dimensions } from 'react-native';
import { inject } from 'mobx-react';
import LinearGradient from 'react-native-linear-gradient';
import { Block, theme, Text } from 'galio-framework';
import { ActivityIndicator, Colors } from 'react-native-paper';
import { HeaderHeight } from "../../constants/utils";

const { width, height } = Dimensions.get('window');

@inject('AuthStore')
export default class Loading extends Component {
    async componentDidMount() {
        await this.props.AuthStore.setupAuth();
    }

    render() {
        return (
            <LinearGradient
				start={{ x: 0, y: 0 }}
				end={{ x: 0.25, y: 1.1 }}
				locations={[0.2, 1]}
				colors={['#3ca4d5', '#003249']}
				style={[styles.home, {flex: 1, paddingTop: theme.SIZES.BASE * 5}]}
            >
                <Block center style={styles.body}>
                    <Image 
                        style={styles.image}
                        source={require('../../images/tmekdisi.png')}
                    />
                    <Text style={{fontSize: 20, marginBottom: 15, color:'white'}}>Takım Ömrü Yönetimi</Text>
                    <ActivityIndicator animating={true} color={Colors.white} />
                </Block>
            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    home: {
        marginTop: Platform.OS === 'android' ? -theme.SIZES.BASE*2 : 0,
    },
    image: {
		width: width * 0.5, 
        height: width * 0.2,
		resizeMode: 'contain',
    },
    body: {
        marginTop: height * 0.25
    }
});
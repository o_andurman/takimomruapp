import React from 'react';
import { StyleSheet, Dimensions, ScrollView, Image, Platform, TouchableWithoutFeedback } from 'react-native';
import { Block, Text, theme, Icon } from 'galio-framework';
import LinearGradient from 'react-native-linear-gradient';
import { Card, Divider, Title, Paragraph } from 'react-native-paper';

import { Images, materialTheme } from '../../constants';
import { HeaderHeight } from "../../constants/utils";

import NotificationIcon from '../../components/NotificationIcon';

const { width } = Dimensions.get('screen');

export default class Profile extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      collapse: {
        1 : true,
        2 : true,
        3 : true,
        4 : true,
        5 : true,
        6 : true,
        7 : true,
        8 : true
      }
    } 
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.getParam('title')+" Parçaları",
      headerRight: () => <NotificationIcon />,
    };
  };

  _collapseActive = async (name) => {
    const { collapse } = this.state;
    collapse[name] = !collapse[name];

    this.setState({ collapse });
  }

  render() {

    return (
      <LinearGradient
				start={{ x: 0, y: 0 }}
				end={{ x: 0.25, y: 1.1 }}
				locations={[0.2, 1]}
				colors={['#3ca4d5', '#003249']}
				style={[styles.home, {flex: 1, paddingTop: theme.SIZES.BASE * 5}]}
      >
        <ScrollView showsVerticalScrollIndicator={false}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 0.25, y: 1.1 }}
            locations={[0.2, 1]}
            colors={['#addeff', '#98c3e0']}
            style={styles.background}
          >
            <Block style={styles.card}>
              <TouchableWithoutFeedback
                onPress={() => this._collapseActive(1)}
              >
                <Block row style={styles.cardTitle}>
                  <Icon name="pencil" family="font-awesome" size={25} color="white" />
                  <Text style={{fontWeight:'bold', fontSize:20, textAlign: 'justify', marginRight: theme.SIZES.BASE, marginLeft: 5 }} color="white">Takım Ömrü uygulamasının kullanım amacı</Text>
                </Block>
              </TouchableWithoutFeedback>
              {!this.state.collapse[1] &&
                <Block style={styles.cardBody}>
                  <Text style={{ fontSize:15, textAlign: 'justify' }} color="white" >
                    Takım Ömrü uygulaması, takımlar ve takımlara ait parçaların anlık durumlarının görüntülenmesini 
                    sağlar. Takımlardaki parçaların saat bazında ne kadar ilerleme kat ettiğini, ömürlerinin bitmesine 
                    saat bazında kaç saat kaldığını, parça bazında ise kaç parça kaldığı, ömrü bitmek üzere olan 
                    parçaların görünebildiği gibi bilgilerin görüntülendiği, takımların ve parçaların eklenebildiği, 
                    silinebildiği, arama ve filtreleme gibi işlemlerin yapılabildiği, tüm takımların ve parçaların excel 
                    olarak çıktılarının alınabildiği çok fonksiyonlu bir sayfadır.
                  </Text>
                </Block>
              }
            </Block>
          </LinearGradient>  
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 0.25, y: 1.1 }}
            locations={[0.2, 1]}
            colors={['#addeff', '#98c3e0']}
            style={styles.background}
          > 
            <Block style={styles.card}>
              <TouchableWithoutFeedback
                onPress={() => this._collapseActive(2)}
              >
                <Block row style={styles.cardTitle}>
                  <Icon name="pencil" family="font-awesome" size={25} color="white" />
                  <Text style={{fontWeight:'bold', fontSize:20, textAlign: 'justify', marginRight: theme.SIZES.BASE, marginLeft: 5 }} color="white">Makineler sayfasının içerdiği fonksiyonlar</Text>
                </Block>
              </TouchableWithoutFeedback>
              {!this.state.collapse[2] &&
                <Block style={styles.cardBody}>
                  <Text style={{ fontSize:15, textAlign: 'justify' }} color="white" >
                    Makineler sayfası makinelerin listelendiği sayfadır. İçerisinde arama fonksiyonu içermektedir.
                  </Text>
                </Block>
              }
            </Block>
          </LinearGradient>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 0.25, y: 1.1 }}
            locations={[0.2, 1]}
            colors={['#addeff', '#98c3e0']}
            style={styles.background}
          >  
            <Block style={styles.card}>
              <TouchableWithoutFeedback
                onPress={() => this._collapseActive(3)}
              >
                <Block row style={styles.cardTitle}>
                  <Icon name="pencil" family="font-awesome" size={25} color="white" />
                  <Text style={{fontWeight:'bold', fontSize:20, textAlign: 'justify', marginRight: theme.SIZES.BASE, marginLeft: 5 }} color="white">Takımlar sayfasının içerdiği fonksiyonlar</Text>
                </Block>
              </TouchableWithoutFeedback>
              {!this.state.collapse[3] &&
                <Block style={styles.cardBody}>
                  <Text style={{ fontSize:15, textAlign: 'justify' }} color="white" >
                    Takımlar sayfası takımların listelendiği sayfadır. İçerisinde ekleme, silme ve arama fonksiyonları mevcuttur.
                  </Text>
                </Block>
              }
            </Block>
          </LinearGradient>   
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 0.25, y: 1.1 }}
            locations={[0.2, 1]}
            colors={['#addeff', '#98c3e0']}
            style={styles.background}
          >
            <Block style={styles.card}>
              <TouchableWithoutFeedback
                onPress={() => this._collapseActive(4)}
              >
                <Block row style={styles.cardTitle}>
                  <Icon name="pencil" family="font-awesome" size={25} color="white" />
                  <Text style={{fontWeight:'bold', fontSize:20, textAlign: 'justify', marginRight: theme.SIZES.BASE, marginLeft: 5 }} color="white">Parçalar sayfasının içerdiği fonksiyonlar</Text>
                </Block>
              </TouchableWithoutFeedback>
              {!this.state.collapse[4] &&
                <Block style={styles.cardBody}>
                  <Text style={{ fontSize:15, textAlign: 'justify' }} color="white" >
                    Parçalar sayfası takımlara ait parçaların listelendiği sayfadır. İçerisinde ekleme, silme ve arama fonksiyonları içermektedir.
                  </Text>
                </Block>
              }
            </Block>
          </LinearGradient>   
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 0.25, y: 1.1 }}
            locations={[0.2, 1]}
            colors={['#addeff', '#98c3e0']}
            style={styles.background}
          >
            <Block style={styles.card}>
              <TouchableWithoutFeedback
                onPress={() => this._collapseActive(5)}
              >
                <Block row style={styles.cardTitle}>
                  <Icon name="pencil" family="font-awesome" size={25} color="white" />
                  <Text style={{fontWeight:'bold', fontSize:20, textAlign: 'justify', marginRight: theme.SIZES.BASE, marginLeft: 5 }} color="white">Bildirimler sayfasının içerdiği fonksiyonlar</Text>
                </Block>
              </TouchableWithoutFeedback>
              {!this.state.collapse[5] &&
                <Block style={styles.cardBody}>
                  <Text style={{ fontSize:15, textAlign: 'justify' }} color="white" >
                    Bildirimler sayfası takımlara ait parçaların ömürlerinin bitmeye yaklaştığındaki bildirimleri gösteren sayfadır.
                  </Text>
                </Block>
              }
            </Block>
          </LinearGradient>   
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 0.25, y: 1.1 }}
            locations={[0.2, 1]}
            colors={['#addeff', '#98c3e0']}
            style={styles.background}
          >
            <Block style={styles.card}>
              <TouchableWithoutFeedback
                onPress={() => this._collapseActive(6)}
              >
                <Block row style={styles.cardTitle}>
                  <Icon name="pencil" family="font-awesome" size={25} color="white" />
                  <Text style={{fontWeight:'bold', fontSize:20, textAlign: 'justify', marginRight: theme.SIZES.BASE, marginLeft: 5 }} color="white">Hesabım sayfasının içerdiği fonksiyonlar</Text>
                </Block>
              </TouchableWithoutFeedback>
              {!this.state.collapse[6] &&
                <Block style={styles.cardBody}>
                  <Text style={{ fontSize:15, textAlign: 'justify' }} color="white" >
                    Hesabım sayfası firma bilgilerinin olduğu, bilgilerin güncellenebildiği, takım veya takıma ait parçalar için oluşturulmuş notların listelendiği sayfadır.
                  </Text>
                </Block>
              }
            </Block>
          </LinearGradient>   
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 0.25, y: 1.1 }}
            locations={[0.2, 1]}
            colors={['#addeff', '#98c3e0']}
            style={styles.background}
          >
            <Block style={styles.card}>
              <TouchableWithoutFeedback
                onPress={() => this._collapseActive(7)}
              >
                <Block row style={styles.cardTitle}>
                  <Icon name="pencil" family="font-awesome" size={25} color="white" />
                  <Text style={{fontWeight:'bold', fontSize:20, textAlign: 'justify', marginRight: theme.SIZES.BASE, marginLeft: 5 }} color="white">Takımlarımızı nasıl ekleriz?</Text>
                </Block>
              </TouchableWithoutFeedback>
              {!this.state.collapse[7] &&
                <Block style={styles.cardBody}>
                  <Text style={{ fontSize:15, textAlign: 'justify' }} color="white" >
                    Takımlar sayfasında sağ alt tarafta bulunan düzenle butonuna bastığınızda, takım ekle seçeneğine basarak yeni takım ekleyebilirsiniz. Ayrıca her takımın gösterildiği kartın sol alt tarafından silebilir, sağ alt tarafından da not ekleyebilirsiniz.
                  </Text>
                </Block>
              }
            </Block>
          </LinearGradient>  
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 0.25, y: 1.1 }}
            locations={[0.2, 1]}
            colors={['#addeff', '#98c3e0']}
            style={styles.background}
          > 
            <Block style={styles.card}>
              <TouchableWithoutFeedback
                onPress={() => this._collapseActive(8)}
              >
                <Block row style={styles.cardTitle}>
                  <Icon name="pencil" family="font-awesome" size={25} color="white" />
                  <Text style={{fontWeight:'bold', fontSize:20, textAlign: 'justify', marginRight: theme.SIZES.BASE, marginLeft: 5 }} color="white">Parçalarımızı nasıl ekleriz?</Text>
                </Block>
              </TouchableWithoutFeedback>
              {!this.state.collapse[8] &&
                <Block style={styles.cardBody}>
                  <Text style={{ fontSize:15, textAlign: 'justify' }} color="white" >
                    Parçalar sayfasında sağ alt tarafta bulunan düzenle butonuna bastığınızda, parça ekle seçeneğine basarak yeni parça ekleyebilirsiniz. Ayrıca her parçanın gösterildiği kartın sol alt tarafından silebilir, sağ alt tarafından da not ekleyebilirsiniz.
                  </Text>
                </Block>
              }
            </Block>
          </LinearGradient>   
        </ScrollView>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  home: {
    marginTop: Platform.OS === 'android' ? -theme.SIZES.BASE*2 : 0,
  },
  card: {
    backgroundColor: 'rgba(256, 256, 256, 0.2)',
    borderRadius: 15,
    borderWidth: 0,
  },
  background: {
    backgroundColor: 'rgba(256, 256, 256, 0.2)',
    marginHorizontal: theme.SIZES.BASE,
    marginVertical: theme.SIZES.BASE / 2,
    borderRadius: 15,
  },
  cardTitle: {
    paddingHorizontal: theme.SIZES.BASE,
    marginVertical: theme.SIZES.BASE / 2,
  },
  cardBody: {
    marginHorizontal: theme.SIZES.BASE,
    marginBottom: theme.SIZES.BASE / 2,
  }
});

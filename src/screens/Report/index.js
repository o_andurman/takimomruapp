import React, { Component } from 'react';
import { StyleSheet, Image, Dimensions } from 'react-native';
import { inject } from 'mobx-react';
import LinearGradient from 'react-native-linear-gradient';
import { Block, theme, Text, Icon } from 'galio-framework';
import { ActivityIndicator, Colors } from 'react-native-paper';
import { HeaderHeight } from "../../constants/utils";
import { WebView } from 'react-native-webview';
import AuthStore from '../../store/AuthStore';

const { width, height } = Dimensions.get('window');

@inject('AuthStore')
export default class Loading extends Component {
    async componentDidMount() {
        await this.props.AuthStore.setupAuth();
    }

    render() {
        return (
            <LinearGradient
				start={{ x: 0, y: 0 }}
				end={{ x: 0.25, y: 1.1 }}
				locations={[0.2, 1]}
				colors={['#3ca4d5', '#003249']}
				style={[styles.home, {flex: 1, paddingTop: theme.SIZES.BASE * 5}]}
            >
                <Block flex>
                    {AuthStore.user == "pimsun" ?
                    <WebView
                    startInLoadingState={true}
                    style={{ flex: 1 }}
                    automaticallyAdjustContentInsets={false}
                    overScrollMode="never"
                    bounces={false}
                    hideKeyboardAccessoryView={true}
                    source={{uri: 'https://toolsapp.kapasitematik.com/Home/EmbedReportMobile'}}
                    />
                    :
                    <Block style={styles.emty}>
                        <Icon name="info-circle" family="font-awesome" size={50} color="white" style={{ marginVertical: 5, marginTop: theme.SIZES.BASE * 10}}/>
                        <Text style={{color:'white'}}>Herhangi bir rapor bulunamadı.</Text>
                    </Block>
                    }
                    
                </Block>
            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    home: {
        marginTop: Platform.OS === 'android' ? -theme.SIZES.BASE*2 : 0,
    },
    image: {
		width: width * 0.5, 
        height: width * 0.2,
		resizeMode: 'contain',
    },
    body: {
        marginTop: height * 0.25
    },
    emty: {
        paddingHorizontal: theme.SIZES.BASE,
        backgroundColor: 'rgba(0, 0, 0, 0.01)',
        alignItems: 'center'
    },
});
import React, { Component } from 'react';
import { StyleSheet, Dimensions, ScrollView, TouchableWithoutFeedback, SafeAreaView, Alert } from 'react-native';
import { Block, Text, theme, Icon, Input, Button } from 'galio-framework';
import { materialTheme } from '../../constants/';
import LinearGradient from 'react-native-linear-gradient';
import NavigationService from '../../NavigationService';
import { FAB, Portal, Provider, Card, Divider } from 'react-native-paper';

import HttpService from '../../utils/HttpService';
import { HeaderHeight } from "../../constants/utils";
import NotificationIcon from '../../components/NotificationIcon';
import ProfileIcon from '../../components/ProfileIcon';

const { width } = Dimensions.get('screen');

// pages
import Loading from '../Loading';

export default class Machines extends Component {
    constructor(props){
        super(props);
        this.state = {
          item: 7,
          isLoading: true,
          categories: [],
          filteredCategories: [],
          search: '',
          active: false,
          formModalVisible: false,
          noteFormVisible: false,
          activePieceName: null,
          activePieceId: null,
          open: false,
        } 
    }

    componentDidMount() {
        if(this.state.categories.length == 0)
        {
            this._retrieveMachines();
        }
    }

    _retrieveMachines = async () => {
        const result = await HttpService.getMachines();
        if(!result || result == false) {
            this.setState({
                categories: [],
                filteredCategories: [],
                isLoading: false
            })
        }else{
            this.setState({
                isLoading: false,
                categories: result,
                filteredCategories: [],
                item: 7,
            })
        }
    }

    _onStateChange = ({ open }) => this.setState({ open });

    _handleSearchChange = (search) => {
        const filteredCategories = this.state.categories.filter(item => search.toLowerCase() && item.machineName.toLowerCase().includes(search.toLowerCase()));
        this.setState({ filteredCategories, search });
    }

    _increaseList = () => {
        const dataLength = this.state.categories.length;
        const item = this.state.item;
        if(this.state.item < dataLength)
        {
            this.setState({item: item + 2})
            return false
        }
    }

    _renderSearch = () => {
        const { search } = this.state;
        const iconSearch = (search ?
          <TouchableWithoutFeedback onPress={() => this.setState({ search: '' })}>
            <Icon size={16} color={theme.COLORS.MUTED} name="page-remove" family="foundation" />
          </TouchableWithoutFeedback> :
          <Icon size={16} color={theme.COLORS.MUTED} name="magnifying-glass" family="entypo" />
        )
        return (
            <Block >
                <Input
                    right
                    color="black"
                    autoFocus={false}
                    autoCorrect={false}
                    autoCapitalize="none"
                    iconContent={iconSearch}
                    defaultValue={search}
                    style={[styles.search]}
                    placeholderTextColor={materialTheme.COLORS.BLACK}
                    placeholder="Takımlar içerisinde arama yapın."
                    onFocus={() => this.setState({ active: true })}
                    onBlur={() => this.setState({ active: false })}
                    onChangeText={this._handleSearchChange}
                />
            </Block>
        )
    }

    _renderCategories() {
        if(this.state.filteredCategories.length != 0){
            return this.state.filteredCategories.map((items) => 
                <TouchableWithoutFeedback
                key={items.machineID}
                onPress={() => NavigationService.navigate('Home', {
                    title: items.machineName,
                    machineID: items.deviceNo
                })}
                >
                    <Card key={items.pieceId} style={styles.category}>
                        <LinearGradient
                            start={{ x: 0, y: 0 }}
                            end={{ x: 0.25, y: 1.1 }}
                            locations={[0.2, 1]}
                            colors={['#addeff', '#98c3e0']}
                            style={styles.gradient}
                        >
                            <Block style={styles.categoryTitle}>
                                <Block row>
                                    <Block flex row left>
                                        <Icon name="cogs" family="font-awesome" size={15} color="rgba(0, 0, 0, 0.7)" style={{ marginRight: 5}}/>
                                        <Text style={{fontWeight:'bold', fontSize:15}} color="rgba(0, 0, 0, 0.7)">{items.machineName}</Text>
                                    </Block>
                                    <Block flex right>
                                        <Block row>
                                            <Block>
                                                <Text color="rgba(0, 0, 0, 0.7)" style={{fontWeight:'bold'}}>Toplam Takım</Text>
                                            </Block>
                                            <Block>
                                                <Text color="rgba(0, 0, 0, 0.6)" center style={{fontWeight:'bold', marginLeft:5}}>{items.adet}</Text>
                                            </Block>
                                        </Block>
                                    </Block>
                                </Block>
                                <Divider style={{backgroundColor:'black', marginVertical:5}}/>
                                <Block row style={{marginTop: 5, marginBottom: 5}}>
                                    <Block flex left style={{backgroundColor:'rgba(76, 175, 80, 0.8)', borderRadius: 5, marginRight: 5, height: 50, justifyContent:'center'}}>
                                        <Block center >
                                            <Text color="white">İyi Parçalar</Text>
                                            <Text color="white">{items.totalIyiParca}</Text>
                                        </Block>
                                    </Block>
                                    <Block flex center style={{backgroundColor:'rgba(255, 152, 0, 0.8)', borderRadius: 5, marginRight: 2.5, marginLeft: 2.5, height: 50, justifyContent:'center'}}>
                                        <Text color="white">Normal Parçalar</Text>
                                        <Text color="white">{items.totalNormalParca}</Text>
                                    </Block>
                                    <Block flex right style={{backgroundColor:'rgba(244, 67, 54, 0.9)', borderRadius: 5, marginLeft: 5, height: 50, justifyContent:'center'}}>
                                        <Block center>
                                            <Text color="white">Kritik Parçalar</Text>
                                            <Text center color="white">{items.totalKotuParca}</Text>
                                        </Block>
                                    </Block>
                                </Block>
                            </Block>
                        </LinearGradient>
                    </Card>
                </TouchableWithoutFeedback>
            );
        }else if(this.state.categories.length != 0 && this.state.search == '') {
            return this.state.categories.slice(0, this.state.item).map((items) => 
                <TouchableWithoutFeedback
                key={items.machineID}
                onPress={() => NavigationService.navigate('Home', {
                    title: items.machineName,
                    machineID: items.deviceNo
                })}
                >
                    <Card key={items.pieceId} style={styles.category}>
                        <LinearGradient
                            start={{ x: 0, y: 0 }}
                            end={{ x: 0.25, y: 1.1 }}
                            locations={[0.2, 1]}
                            colors={['#addeff', '#98c3e0']}
                            style={styles.gradient}
                        >
                            <Block style={styles.categoryTitle}>
                                <Block row>
                                    <Block flex row left>
                                        <Icon name="cogs" family="font-awesome" size={15} color="rgba(0, 0, 0, 0.7)" style={{ marginRight: 5}}/>
                                        <Text style={{fontWeight:'bold', fontSize:15}} color="rgba(0, 0, 0, 0.7)">{items.machineName}</Text>
                                    </Block>
                                    <Block flex right>
                                        <Block row>
                                            <Block>
                                                <Text color="rgba(0, 0, 0, 0.7)" style={{fontWeight:'bold'}}>Toplam Takım</Text>
                                            </Block>
                                            <Block>
                                                <Text color="rgba(0, 0, 0, 0.6)" center style={{fontWeight:'bold', marginLeft:5}}>{items.adet}</Text>
                                            </Block>
                                        </Block>
                                    </Block>
                                </Block>
                                <Divider style={{backgroundColor:'black', marginVertical:5}}/>
                                <Block row style={{marginTop: 5, marginBottom: 5}}>
                                    <Block flex left style={{backgroundColor:'rgba(76, 175, 80, 0.8)', borderRadius: 5, marginRight: 5, height: 50, justifyContent:'center'}}>
                                        <Block center >
                                            <Text color="white">İyi Parçalar</Text>
                                            <Text color="white">{items.totalIyiParca}</Text>
                                        </Block>
                                    </Block>
                                    <Block flex center style={{backgroundColor:'rgba(255, 152, 0, 0.8)', borderRadius: 5, marginRight: 2.5, marginLeft: 2.5, height: 50, justifyContent:'center'}}>
                                        <Text color="white">Normal Parçalar</Text>
                                        <Text color="white">{items.totalNormalParca}</Text>
                                    </Block>
                                    <Block flex right style={{backgroundColor:'rgba(244, 67, 54, 0.9)', borderRadius: 5, marginLeft: 5, height: 50, justifyContent:'center'}}>
                                        <Block center>
                                            <Text color="white" >Kritik Parçalar</Text>
                                            <Text center color="white">{items.totalKotuParca}</Text>
                                        </Block>
                                    </Block>
                                </Block>
                            </Block>
                        </LinearGradient>
                    </Card>
                </TouchableWithoutFeedback>
            );
        }else {
            return (
                <Block style={styles.emty}>
                    <Icon name="info-circle" family="font-awesome" size={50} color="white" style={{ marginVertical: 5, marginTop: theme.SIZES.BASE * 10}}/>
                    <Text style={{color:'white'}}>Herhangi bir makine bulunamadı.</Text>
                </Block>
            )
        }
    }

    render() {
        const { open } = this.state;

        if (this.state.isLoading) {
            return (
                <Loading />
            )
        }
        return (
            <LinearGradient
				start={{ x: 0, y: 0 }}
				end={{ x: 0.25, y: 1.1 }}
				locations={[0.2, 1]}
				colors={['#3ca4d5', '#003249']}
				style={[styles.home, {flex: 1, paddingTop: theme.SIZES.BASE * 5}]}
            >
                <Block row>
                    <Block style={{justifyContent:'center', alignItems:'center'}}>
                        <ProfileIcon />
                    </Block>
                    {this._renderSearch()}
                    <Block style={{justifyContent:'center', alignItems:'center'}}>
                        <NotificationIcon />
                    </Block>
                </Block>
                <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.categoryList}>
                    {this._renderCategories()}
                    {this.state.item < this.state.categories.length && this.state.filteredCategories.length == 0 ? 
                        <Block center>
                            <Button onPress={this._increaseList} color="rgba(256, 256, 256, 0.3)" style={{ width: 100, height: 30, borderRadius: 15 }}>  Daha Fazla  </Button>
                        </Block>
                        :
                        <Block />
                    }
                </ScrollView>
                <Provider>
                    <Portal>
                        <FAB.Group
                            open={open}
                            icon={open ? 'close' : 'pencil'}
                            actions={[
                                { icon: 'help', label:'Yardım', onPress: () => NavigationService.navigate('Help') },
                                { icon: 'file', label:'Rapor', onPress: () => NavigationService.navigate('Report') },
                            ]}
                            onStateChange={this._onStateChange}
                            onPress={() => {
                            if (open) {
                                // do something if the speed dial is open
                            }
                            }}
                        />
                    </Portal>
                </Provider>
            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    home: {
        marginTop: Platform.OS === 'android' ? -theme.SIZES.BASE*2 : 0,
    },
    categoryList: {
      justifyContent: 'center',
      paddingTop: theme.SIZES.BASE * 0.1,
      paddingBottom: 50
    },
    category: {
      backgroundColor: 'rgba(256, 256, 256, 0.2)',
      marginHorizontal: theme.SIZES.BASE,
      marginVertical: theme.SIZES.BASE / 2,
      borderRadius: 15,
      borderWidth: 0,
    },
    gradient: {
        backgroundColor: 'rgba(256, 256, 256, 0.2)',
        borderRadius: 15,
        borderWidth: 0,
    },
    categoryTitle: {
      paddingHorizontal: theme.SIZES.BASE,
      marginVertical: theme.SIZES.BASE / 2,
    },
    emty: {
        paddingHorizontal: theme.SIZES.BASE,
        backgroundColor: 'rgba(0, 0, 0, 0.01)',
        alignItems: 'center'
    },
    search: {
        height: 40,
        width: width - 112,
        marginHorizontal: theme.SIZES.BASE / 2,
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        borderWidth: 0,
        borderRadius: 15,
    },
    shadowSearch: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 3 },
        shadowRadius: 4,
        shadowOpacity: 0.1,
        elevation: 2,
    },
  });
import React, { Component } from 'react';

import { StyleSheet, Dimensions, KeyboardAvoidingView,TouchableWithoutFeedback, Alert } from "react-native";
import { Block, Text, theme, Icon, Input, Button, Radio } from 'galio-framework';
import { RadioButton, ActivityIndicator, Colors, Divider } from 'react-native-paper';
import { materialTheme } from '../../constants/';
import { TextInputMask } from 'react-native-masked-text';
import HttpService from '../../utils/HttpService';
import moment from "moment";

const { width } = Dimensions.get('screen');

export default class AddForm extends Component {
	constructor(props){
		super(props);
		this.state = {
			subPieceName: null,
			pieceCount: null,
			toollife: null,
			startDate: "",
			endDate: "",
			checked: 'first',
			isSubmitting: false,
			active: {
			  subPieceName: false,
			  pieceCount: false,
			},
			validation: {
				subPieceName: null,
				pieceCount: null,
				startDate: null,
				endDate: null,
				date: null
			}
		}
	}
	
	_handleSubmit = async () => {
		try {
			await this.setSubmitting();
			if( this.state.startDate != "" && this.state.endDate != "" )
			{
				//type: 1
				let validStartDate = await this._checkDate(this.state.startDate);
				let validEndDate = await this._checkDate(this.state.endDate);
				let toollife = await this._dateDiff(this.state.startDate, this.state.endDate)
				if( validStartDate && validEndDate && toollife > 0)
				{
					let response = await HttpService.addSubCategory(this.state.subPieceName, toollife*24, this.props.pieceID, true);
					if(!response)
					{
						alert("Parça eklenirken hata oluştu.")
						this.setSubmitting();
						return false
					}else{
						Alert.alert(   
							'Tebrikler'
							,'Parçanız başarıyla eklendi.'
							,[
								{text: 'Tamam', onPress: () => {
									this.props.retrieveCategories();
									this.props.formModalActive();
									} 
								},
							]
						)
					}
				}else {
					await this.setSubmitting();
					alert("Lütfen geçerli bir tarih aralığı giriniz.")
				}
			}else if( this.state.pieceCount )
			{
				//type: 0
				let response = await HttpService.addSubCategory(this.state.subPieceName, this.state.pieceCount, this.props.pieceID, false);
				if(!response)
				{
					alert("Parça eklenirken hata oluştu.")
					this.setSubmitting();
					return false
				}else{
					Alert.alert(   
						'Tebrikler'
						,'Parçanız başarıyla eklendi.'
						,[
							{text: 'Tamam', onPress: () => {
								this.props.retrieveCategories();
								this.props.formModalActive();
								} 
							},
						]
					)
				}
			}else {
				await this.setSubmitting();
				alert("Lütfen gerekli alanları doldurunuz.")
			}
		}catch (e) {
			alert("Parça eklenirken hata oluştu.")
			this.props.retrieveCategories();
			this.props.formModalActive();
			console.log(e);
		}
	};

	setSubmitting = async () => {
		this.setState({isSubmitting: !this.state.isSubmitting})
	}

	handleChange = (name, value) => {
		this.setState({ [name]: value });
		const { validation } = this.state;
		if(name=="subPieceName")
		{
			//let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			let reg = /^[a-zA-Z0-9 ğçşüöıĞÇŞÜÖİ.-]+$/;
			if(value == '')
			{
				validation["subPieceName"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["subPieceName"] = "*Bu alan sadece a-z, A-Z veya 0-9 karakterlerini içermelidir.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["subPieceName"] = null;
				this.setState({ validation })
			}
		}
		if(name=="pieceCount")
		{
			//let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			let reg = /^[0-9]+$/;
			if(value == '')
			{
				validation["pieceCount"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["pieceCount"] = "*Bu alan sadece rakam içermelidir.";
				this.setState({ validation })
				return false;
			}
			else {
				validation["pieceCount"] = null;
				this.setState({ validation })
			}
		}
		if(name=="startDate")
		{
			//let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			let reg = /^\d{2}[./-]\d{2}[./-]\d{4}$/;
			if(value == '')
			{
				validation["startDate"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["startDate"] = "*Lütfen geçerli bir tarih giriniz. (Gün-Ay-Yıl)";
				this.setState({ validation })
				return false;
			}
			else {
				validation["startDate"] = null;
				this.setState({ validation })
			}
		}
		if(name=="endDate")
		{
			//let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			let reg = /^\d{2}[./-]\d{2}[./-]\d{4}$/;
			if(value == '')
			{
				validation["endDate"] = null;
				this.setState({ validation });
				return false;
			}
			if (reg.test(value) === false && value!=null) {
				validation["endDate"] = "*Lütfen geçerli bir tarih giriniz. (Gün-Ay-Yıl)";
				this.setState({ validation })
				return false;
			}
			else {
				validation["endDate"] = null;
				this.setState({ validation })
			}
		}
	}

	_checkDate = async ( date ) => {
		tempDate = date.split("-");
		tempDate.reverse();
		reverseDate = tempDate.join("-");
		const dateFormat = 'DD-MM-YYYY';
		const toDateFormat = moment(new Date(reverseDate)).format(dateFormat);
		return moment(toDateFormat, dateFormat, true).isValid();
	}

	_dateDiff = async ( startDate, endDate) => {
		tempDate = startDate.split("-");
		tempDate.reverse();
		reverseDate = tempDate.join("-");
		tempDate2 = endDate.split("-");
		tempDate2.reverse();
		reverseDate2 = tempDate2.join("-");
		const a = moment(reverseDate);
		const b = moment(reverseDate2);
		const msDiff = b.diff(a, 'days');
		return msDiff
	}

	toggleActive = (name) => {
		const { active } = this.state;
		active[name] = !active[name];

		this.setState({ active });
	}

	render() {
		const { checked } = this.state;
		return (
			//Aşağıda kart boyutu olasılıklara göre yeniden boyutlandırıldı.
			<Block center style={[styles.cart]}>
				<Block  middle>
					<KeyboardAvoidingView behavior="padding" enabled>
						<Block row middle style={{ paddingVertical: theme.SIZES.BASE * 0.625 }}>
							<Icon name="wrench" family="font-awesome" size={20} color="white" style={{marginRight:5}}/>
							<Text center color="white" size={theme.SIZES.FONT * 1.5}>
								Parça Ekle
							</Text>
						</Block>
						<Divider style={{backgroundColor:'white'}}/>
						<Block >
							<Block middle>
								<Text
								color={theme.COLORS.WHITE}
								size={theme.SIZES.FONT }
								style={{ alignSelf: 'flex-start', lineHeight: theme.SIZES.FONT * 2 }}
								>
									Takım Ömrü Tipi:
								</Text>
							</Block>	
							<Block row>
								<RadioButton
									value="first"
									status={checked === 'first' ? 'checked' : 'unchecked'}
									color="white"
									onPress={() => { this.setState({ checked: 'first', pieceCount: null, startDate:"", endDate:"" }); }}
								/>
								<TouchableWithoutFeedback
									onPress={() => this.setState({ checked: 'first', pieceCount: null, startDate:"", endDate:"" })}
								>
									<Text
									color={theme.COLORS.WHITE}
									size={theme.SIZES.FONT }
									style={{ alignSelf: 'flex-start', lineHeight: theme.SIZES.FONT * 2 }}
									>
										Parça Sayısına Göre
									</Text>
								</TouchableWithoutFeedback>
								<RadioButton
									value="second"
									status={checked === 'second' ? 'checked' : 'unchecked'}
									color="white"
									onPress={() => { this.setState({ checked: 'second', pieceCount: null, startDate:"", endDate:"" }); }}
								/>
								<TouchableWithoutFeedback
									onPress={() => this.setState({ checked: 'second', pieceCount: null, startDate:"", endDate:"" })}
								>
									<Text
									color={theme.COLORS.WHITE}
									size={theme.SIZES.FONT }
									style={{ alignSelf: 'flex-start', lineHeight: theme.SIZES.FONT * 2 }}
									>
										Zamana Göre
									</Text>
								</TouchableWithoutFeedback>
							</Block>
							<Block middle>	
								<Text
								color={theme.COLORS.WHITE}
								size={theme.SIZES.FONT }
								style={{ alignSelf: 'flex-start', lineHeight: theme.SIZES.FONT * 2 }}
								>
									Parça Adı:
								</Text>
								<Input
								color="white"
								placeholder="Parça Adı Giriniz"
								autoCapitalize="none"
								autoCorrect={false}
								bgColor='transparent'
								onBlur={() => this.toggleActive('subPieceName')}
								onFocus={() => this.toggleActive('subPieceName')}
								placeholderTextColor={materialTheme.COLORS.WHITE}
								onChangeText={text => this.handleChange('subPieceName', text)}
								style={[styles.input, this.state.active.subPieceName ? styles.inputActive : null]}
								/>
								{this.state.validation.subPieceName != null &&
									<Text color="orange" size={14}>{this.state.validation.subPieceName}</Text>
								}
								{this.state.checked == 'first' && 
									<Block middle>
										<Text
										color={theme.COLORS.WHITE}
										size={theme.SIZES.FONT }
										style={{ alignSelf: 'flex-start', lineHeight: theme.SIZES.FONT * 2 }}
										>
											Takım Ömrü (Parça Sayısı):
										</Text>
										<Input
										color="white"
										placeholder="Parça Adetini Giriniz"
										autoCapitalize="none"
										autoCorrect={false}
										bgColor='transparent'
										onBlur={() => this.toggleActive('pieceCount')}
										onFocus={() => this.toggleActive('pieceCount')}
										placeholderTextColor={materialTheme.COLORS.WHITE}
										onChangeText={text => this.handleChange('pieceCount', text)}
										style={[styles.input, this.state.active.pieceCount ? styles.inputActive : null]}
										/>
										{this.state.validation.pieceCount != null &&
											<Text color="orange" size={14}>{this.state.validation.pieceCount}</Text>
										}
									</Block>
								}
								{this.state.checked == 'second' && 
									<Block middle style={{marginBottom:10}}>
										<Text
										color={theme.COLORS.WHITE}
										size={theme.SIZES.FONT }
										style={{ alignSelf: 'flex-start', lineHeight: theme.SIZES.FONT * 2 }}
										>
											Zaman Aralığını Giriniz:
										</Text>
										<Block row space="evenly" style={{marginTop:10, marginBottom:10}}>
											<Block flex row right>
												<Icon name="calendar" family="font-awesome" size={25} color="white" style={{marginTop:3}}/>
												<TextInputMask
												type={'datetime'}
												options={{
													format: 'DD-MM-YYYY',
												}}
												style={styles.dateInput}
												value={this.state.startDate}
												placeholder="GG-AA-YYYY"
												placeholderTextColor='white'
												onChangeText={text => {
													this.handleChange('startDate', text)
												}}
												/>
											</Block>
											<Block flex row right>
											<Icon name="calendar" family="font-awesome" size={25} color="white" style={{marginTop:3}}/>
												<TextInputMask
												type={'datetime'}
												options={{
													format: 'DD-MM-YYYY',
												}}
												style={styles.dateInput}
												value={this.state.endDate}
												placeholder="GG-AA-YYYY"
												placeholderTextColor='white'
												onChangeText={text => {
													this.handleChange('endDate', text)
												}}
												/>
											</Block>
										</Block>
										{this.state.validation.startDate != null &&
											<Text color="orange" size={14}>{this.state.validation.startDate}</Text>
										}
										{this.state.validation.endDate != null &&
											<Text color="orange" size={14}>{this.state.validation.endDate}</Text>
										}
									</Block>
								}
							</Block>
							<Block top row space="evenly" style={{marginTop: 10, marginBottom: 10}}>
								<Block flex left>
									<Button
									shadowless
									color="rgba(244, 67, 54, 0.9)"
									style={{ height: 36, width: (width - 32)*0.4, borderRadius: 20 }}
									onPress={() => this.props.formModalActive()}
									>
									VAZGEÇ
									</Button>
								</Block>
								<Block flex right>
									{this.state.isSubmitting ? 
										<ActivityIndicator animating={true} color={Colors.red800} style={{alignSelf:'center'}}/>
									:
									<Button
									shadowless
									color={this.state.subPieceName != '' && !this.state.subPieceName ? materialTheme.COLORS.DEFAULT : materialTheme.COLORS.TEZMAKSAN}
									style={{ height: 36, width: (width - 32)*0.4, borderRadius: 20 }}
									onPress={() => this._handleSubmit()}
									>
									KAYDET
									</Button>
									}
								</Block>
								
							</Block>
						</Block>
					</KeyboardAvoidingView>
				</Block>
            </Block>
		);
	}
}

const styles = StyleSheet.create({
    cart: {
		width: width - 32,
		marginHorizontal: theme.SIZES.BASE,
		borderWidth: 1,
		borderColor: 'rgba(94, 109, 114, 0.4)',
        borderRadius: 20,
		backgroundColor: 'rgba(94, 109, 114, 0.7)'
	},
	input: {
		width: width * 0.8, 
		borderRadius: 5,
		borderBottomWidth: 1,
		borderColor: materialTheme.COLORS.WHITE,
	},
	dateInput: {
		width: width * 0.3, 
		borderRadius: 5,
		borderWidth:1,
		borderColor: materialTheme.COLORS.WHITE,
		marginLeft: 7,
		color: 'white'
	},
	inputActive: {
		borderBottomColor: "white",
	},
	shadow: {
		shadowColor: 'white',
		shadowOffset: { width: 0, height: 2 },
		shadowRadius: 4,
		shadowOpacity: 0.2,
		elevation: 2,
	},
});
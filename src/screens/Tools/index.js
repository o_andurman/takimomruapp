import React, { Component } from 'react';
import { StyleSheet, Dimensions, ScrollView, TouchableWithoutFeedback, SafeAreaView, Alert } from 'react-native';
import { Block, Text, theme, Icon, Input, Button } from 'galio-framework';
import { materialTheme } from '../../constants/';
import LinearGradient from 'react-native-linear-gradient';
import NavigationService from '../../NavigationService';
import SegmentedProgressBar from 'react-native-segmented-progress-bar';
import { VictoryBar, VictoryChart, VictoryAxis, VictoryTheme, VictoryLabel } from "victory-native";
import { FAB, Portal, Provider, Card, Divider, ActivityIndicator, Colors } from 'react-native-paper';

import HttpService from '../../utils/HttpService';
import { HeaderHeight } from "../../constants/utils";
import { HubConnectionBuilder, LogLevel } from '@aspnet/signalr';
import NotificationIcon from '../../components/NotificationIcon';
import { SwipeListView } from 'react-native-swipe-list-view';

import Modal from 'react-native-modal';

const { width } = Dimensions.get('screen');
const hubConnection= new HubConnectionBuilder()
    .withUrl("http://apiselect.kapasitematik.com/signalrHub")
    .build()

// pages
import AddForm from './AddForm';
import AddNewNote from './AddNewNote';
import Loading from '../Loading';

export default class Home extends Component {
    constructor(props){
        super(props);
        this.state = {
          isLoading: true,
          filterLoading: false,
          categories: [],
          collapse: {},
          filteredCategories: [],
          subCategoryDetail: [],
          search: '',
          active: false,
          activeButton: 1,
          formModalVisible: false,
          open: false,
          noteFormVisible: false,
          activeSubPieceName: null,
          activeSubPieceId: null,
          uniqueValue: 1
        } 
        this.formModalActive = this._formModalActive.bind(this)
        this.retrieveCategories = this._renderCategories.bind(this)
        this.noteModalActive = this._noteModalActive.bind(this)
    }

    static navigationOptions = ({ navigation }) => {
        return {
          title: navigation.getParam('title')+" Parçaları",
        };
    };

    componentDidMount() {
        if(this.state.categories.length == 0)
        {
            this._retrieveCategories()
        }
    }

    _onStateChange = ({ open }) => this.setState({ open });

    _handleSearchChange = (search) => {
        const filteredCategories = this.state.categories.filter(item => search.toLowerCase() && item.subPieceName.toLowerCase().includes(search.toLowerCase()));
        this.setState({ filteredCategories, search });
    }

    _retrieveCategories = async () => {
        const pieceID = this.props.navigation.getParam('pieceId');
        const result = await HttpService.getSubCategories(pieceID);
        console.log(result)
        if(!result || result == false) {
            this.setState({
                categories: [],
                filteredCategories: [],
                isLoading: false
            })
        }else{
            const { collapse } = this.state;
            let data = []
            result.map((items) => {
                collapse[items.subPieceId] = true;
                data.push({"key": `${items.subPieceId}`, "createdDate": items.createdDate , "pieceCount": items.pieceCount , "pieceID": items.pieceID , "subPieceName": items.subPieceName , "toolLife": items.toolLife, "type": items.type })
            });
            this.setState({
                isLoading: false,
                categories: data,
                filteredCategories: [],
                activeButton: 1,
                collapse
            })
            await this._endHubConnection()
            this._hubConnection()
        }
    }

    _endHubConnection = async () => {
        hubConnection.stop().then(
            console.log("Connection stopped.")
        ).catch(e => {
            console.log(e)
        })
    }

    _hubConnection = async () => {
        hubConnection
            .start()
            .then(() => {
                console.log('Connection started!')
                hubConnection.on('ParcaSigr', message => {
                    //console.log(message)
                    let data = []
                    message.map((items) => {
                        data.push({"key": `${items.subPieceId}`, "createdDate": items.createdDate , "pieceCount": items.pieceCount , "pieceID": items.pieceID , "subPieceName": items.subPieceName , "toolLife": items.toolLife })
                    });
                    this.setState({categories:data})
                    //this._retrieveCategories();
                })

                hubConnection.onclose(()=>{
                    this.setState({uniqueValue:this.state.uniqueValue + 1})
                })
            })
            .catch(err => console.log('Error while establishing connection: ', err));
    }

    _handleFilter = async ( id ) => {
        this.setState({filterLoading: true})
        const pieceID = this.props.navigation.getParam('pieceId');
        if(id == 1){
            const result = await HttpService.getSubCategories(pieceID);
            if(!result || result == false) {
                this.setState({
                    filterLoading: false
                })
            }else{
                const { collapse } = this.state;
                let data = []
                result.map((items) => {
                    collapse[items.subPieceId] = true;
                    data.push({"key": `${items.subPieceId}`, "createdDate": items.createdDate , "pieceCount": items.pieceCount , "pieceID": items.pieceID , "subPieceName": items.subPieceName , "toolLife": items.toolLife })
                });
                this.setState({
                    filterLoading: false,
                    categories: data,
                    activeButton: 1,
                    collapse
                })
            }
        }else if( id == 2 ){
            const result = await HttpService.getGoodPiece(pieceID);
            if(!result || result == false) {
                this.setState({
                    filterLoading: false
                })
            }else{
                const { collapse } = this.state;
                let data = []
                result.map((items) => {
                    collapse[items.subPieceId] = true;
                    data.push({"key": `${items.subPieceId}`, "createdDate": items.createdDate , "pieceCount": items.pieceCount , "pieceID": items.pieceID , "subPieceName": items.subPieceName , "toolLife": items.toolLife })
                });
                this.setState({
                    filterLoading: false,
                    categories: data,
                    activeButton: 2,
                    collapse
                })
            }
        }else if( id == 3 ){
            const result = await HttpService.getNormalPiece(pieceID);
            if(!result || result == false) {
                this.setState({
                    filterLoading: false
                })
            }else{
                const { collapse } = this.state;
                let data = []
                result.map((items) => {
                    collapse[items.subPieceId] = true;
                    data.push({"key": `${items.subPieceId}`, "createdDate": items.createdDate , "pieceCount": items.pieceCount , "pieceID": items.pieceID , "subPieceName": items.subPieceName , "toolLife": items.toolLife })
                });
                this.setState({
                    filterLoading: false,
                    categories: data,
                    activeButton: 3,
                    collapse
                })
            }
        }else if( id == 4 ){
            const result = await HttpService.getCriticPiece(pieceID);
            if(!result || result == false) {
                this.setState({
                    filterLoading: false
                })
            }else{
                const { collapse } = this.state;
                let data = []
                result.map((items) => {
                    collapse[items.subPieceId] = true;
                    data.push({"key": `${items.subPieceId}`, "createdDate": items.createdDate , "pieceCount": items.pieceCount , "pieceID": items.pieceID , "subPieceName": items.subPieceName , "toolLife": items.toolLife })
                });
                this.setState({
                    filterLoading: false,
                    categories: data,
                    activeButton: 4,
                    collapse
                })
            }
        }else {
            return false
        }
    }

    _collapseActive = async (name) => {
        const { collapse } = this.state;
        if(collapse[name] == false)
        {
            collapse[name] = !collapse[name];
            this.setState({ collapse, subCategoryDetail: [] });
        }else{
            Object.keys(collapse).map((item) => {
                collapse[item] = true
            })
            collapse[name] = !collapse[name];
    
            this.setState({ collapse, subCategoryDetail: [] });
            await this._getSubCategoryDetail(name);
        }
    }

    _formModalActive = () => {
        this.setState({formModalVisible: !this.state.formModalVisible})
    }

    _noteModalActive = (activeSubPieceName, activeSubPieceId) => {
        this.setState({noteFormVisible: !this.state.noteFormVisible, activeSubPieceName: activeSubPieceName, activeSubPieceId:activeSubPieceId})
    }

    _fortmatDate(date) {
        var options = { year: 'numeric', month: 'long', day: 'numeric', timeZone:'UTC'};
        let tempDate = new Date(date).toLocaleString('tr-TR', options);
        return tempDate.split(' ')[0]+ " " + tempDate.split(' ')[1];
    }

    _getSubCategoryDetail = async (id) => {
        try{
            const result = await HttpService.getSubCategoryDetail(id);
            let { subCategoryDetail } = this.state;
            console.log(result)
            if(!result || result == false) {
                return false;
            }else{
                result.map((item) => {
                    subCategoryDetail.push({"quarter": this._fortmatDate(item.createdDate), "earnings": item.pieceCount, "label": item.pieceCount})
                })
                this.setState({
                    subCategoryDetail
                })
            }
        }catch(e){
            console.log(e)
        }
    }
    
    _renderSearch = () => {
        const { search } = this.state;
        const iconSearch = (search ?
          <TouchableWithoutFeedback onPress={() => this.setState({ search: '' })}>
            <Icon size={16} color={theme.COLORS.MUTED} name="page-remove" family="foundation" />
          </TouchableWithoutFeedback> :
          <Icon size={16} color={theme.COLORS.MUTED} name="magnifying-glass" family="entypo" />
        )
        return (
            <Input
                right
                color="black"
                autoFocus={false}
                autoCorrect={false}
                autoCapitalize="none"
                iconContent={iconSearch}
                defaultValue={search}
                style={[styles.search]}
                placeholderTextColor={materialTheme.COLORS.BLACK}
                placeholder="Parçalar içerisinde arama yapın."
                onFocus={() => this.setState({ active: true })}
                onBlur={() => this.setState({ active: false })}
                onChangeText={this._handleSearchChange}
            />
        )
    } 

    _deleteCategory = async (id) => {
        try {
            let response = await HttpService.deleteSubCategory(id);
			if(!response)
			{
				this._retrieveCategories();
				return false
			}else{
                this._retrieveCategories();
                return false
			}
		}catch (e) {
			console.log("Couldn't delete subCategory.");
		}
    }

    _renderFilter = () => {
        if(this.state.categories.length != 0)
        {
            return (
                <Block>
                    {this.state.filterLoading ? 
                        <ActivityIndicator animating={true} color={Colors.white} style={{alignSelf:'center'}}/>
                    :
                        <Block row style={{marginHorizontal: theme.SIZES.BASE }}>
                            <Button shadowless round onPress={async () => await this._handleFilter(1)} style={[styles.filter, {backgroundColor: this.state.activeButton == 1 ? "#00BCD4" : "#DCDCDC"}]}>TÜMÜ</Button>
                            <Button shadowless round onPress={async () => await this._handleFilter(2)} style={[styles.filter, {backgroundColor: this.state.activeButton == 2 ? "#4CAF50" : "#DCDCDC"}]}>İYİ</Button>
                            <Button shadowless round onPress={async () => await this._handleFilter(3)} style={[styles.filter, {backgroundColor: this.state.activeButton == 3 ? "#FF9800" : "#DCDCDC"}]}>NORMAL</Button>
                            <Button shadowless round onPress={async () => await this._handleFilter(4)} style={[styles.filter, {backgroundColor: this.state.activeButton == 4 ? "#F44336" : "#DCDCDC"}]}>KRİTİK</Button>
                        </Block> 
                    }
                </Block>
            )
        }
    }

    _renderCategories() {
        if(this.state.filteredCategories.length != 0){
            return (
                <SwipeListView
                showsVerticalScrollIndicator={false}
                //disableRightSwipe={true}
                ListFooterComponent={this.state.item < this.state.categories.length && this.state.filteredCategories.length == 0 ? 
                    <Block center>
                        <Button onPress={this._increaseList} color="rgba(256, 256, 256, 0.3)" style={{ width: 100, height: 30, borderRadius: 15 }}>  Daha Fazla  </Button>
                    </Block>
                    :
                    <Block />
                }
                data={this.state.filteredCategories}
                renderItem={ (data, rowMap) => (
                    <LinearGradient
                    start={{ x: 0, y: 0 }}
                    end={{ x: 0.25, y: 1.1 }}
                    locations={[0.2, 1]}
                    colors={['#addeff', '#98c3e0']}
                    style={styles.category}
                >
                    <Block key={data.item.key}>
                        <Block style={styles.categoryTitle}>
                            <TouchableWithoutFeedback
                                onPress={() => this._collapseActive(data.item.key)}
                            >
                                <Block row space="between" style={{ marginVertical: theme.SIZES.BASE / 2 }}>
                                    <Block row>
                                        <Icon name="wrench" family="font-awesome" size={15} color="black" />
                                        <Text size={15} bold color={theme.COLORS.BLACK} style={{ paddingLeft:7 }}>{data.item.subPieceName}</Text>
                                    </Block>
                                    <Block row>
                                        {this.state.collapse[data.item.key] ? 
                                            <Icon name="angle-left" family="font-awesome" size={20} color="black"/>
                                            :
                                            <Icon name="angle-down" family="font-awesome" size={20} color="black"/>
                                        }
                                    </Block>
                                </Block>
                            </TouchableWithoutFeedback>
                            <Divider style={{backgroundColor:'black'}}/>
                            {this.state.collapse[data.item.key] ? 
                                <Block >
                                    <Block style={{ marginVertical: theme.SIZES.BASE / 2 }}>
                                        {data.item.type ? 
                                            <Text size={17} color={theme.COLORS.BLACK} >{data.item.pieceCount} Dakika</Text>
                                        :
                                            <Text size={17} color={theme.COLORS.BLACK} >{data.item.pieceCount}. Parça</Text>
                                        }
                                    </Block>
                                    <Block style={{ marginVertical: theme.SIZES.BASE / 2 }}>
                                        <SegmentedProgressBar
                                            values={[0, data.item.toolLife*0.33 + 1, data.item.toolLife*0.66 + 1, data.item.toolLife]}
                                            colors={['green', 'orange', 'red']}
                                            labels={['iyi','normal','kritik']}
                                            position={data.item.pieceCount}
                                        />
                                    </Block>
                                </Block>
                                :
                                <Block >
                                    <Block center style={{ marginVertical: theme.SIZES.BASE / 2 }}>
                                        {this.state.subCategoryDetail.length == 0 ? 
                                        <Text size={25} color={theme.COLORS.BLACK} >Yükleniyor...</Text>
                                        :
                                        <Text size={20} color={theme.COLORS.BLACK} >Son {this.state.subCategoryDetail.length} Gün</Text>
                                        }
                                    </Block>
                                    {this.state.subCategoryDetail === [] ? 
                                    <ActivityIndicator animating={true} color={Colors.white} style={{alignSelf:'center', marginBottom: 10}}/>
                                    :
                                    <Block style={{marginLeft: -20, marginTop: -30}}>
                                        <VictoryChart
                                            // domainPadding will add space to each side of VictoryBar to
                                            // prevent it from overlapping the axis
                                            width={width}
                                            domainPadding={30}
                                            theme={VictoryTheme.material}
                                        >
                                            <VictoryAxis
                                                // tickValues specifies both the number of ticks and where
                                                // they are placed on the axis
                                                tickValues={[1, 2, 3, 4, 5]}
                                                tickFormat={(y) => (`${y}`)}
                                                style={{
                                                    axis: {stroke: "rgba(56, 56, 56, 0.7)"},
                                                    axisLabel: {fontSize: 20, color:'rgba(56, 56, 56, 0.7)'},
                                                    ticks: {stroke: "rgba(56, 56, 56, 0.7)", size: 5},
                                                    tickLabels: {fontSize: 15, padding: 5, fill: 'rgba(56, 56, 56, 0.7)'}
                                                }}
                                            />
                                            <VictoryAxis
                                                dependentAxis
                                                // tickFormat specifies how ticks should be displayed
                                                tickFormat={(x) => (`${x}`)}
                                                style={{
                                                    axis: {stroke: "rgba(25, 56, 56, 0.7)"},
                                                    axisLabel: {fontSize: 20, color:'rgba(56, 56, 56, 0.7)'},
                                                    ticks: {stroke: "rgba(56, 56, 56, 0.7)", size: 5},
                                                    tickLabels: {fontSize: 15, padding: 5, fill: 'rgba(56, 56, 56, 0.7)'}
                                                }}
                                            />
                                            <VictoryBar
                                                data={this.state.subCategoryDetail}
                                                x="quarter"
                                                y="earnings"
                                                width= {10}
                                                style= {{
                                                    data: { fill: 'rgba(56, 56, 56, 0.7)', width: 10 },
                                                    labels: { fill: 'rgba(56, 56, 56, 1)' }
                                                }}
                                                labelComponent={<VictoryLabel dx={15} dy={10}/>}
                                            />
                                        </VictoryChart>
                                    </Block>
                                    }
                                </Block>
                            }
                        </Block>
                    </Block>
                </LinearGradient>
                )}
                renderHiddenItem={ (data, rowMap) => (
                    <Block flex row style={[styles.hidden, {justifyContent:'space-between'}]}>
                        <TouchableWithoutFeedback
                            onPress={()=> this._noteModalActive(data.item.subPieceName,data.item.key)}
                        >
                            <Block left style={{justifyContent:'center', alignItems:'center', width: 100, borderRadius:15, backgroundColor:'rgba(0, 80, 255, 0.8)'}}>
                                <Icon size={25} color="#FFF" style={{ marginRight: 25 }} name="sticky-note" family="font-awesome" />
                                <Text color="white" style={{ marginRight: 25 }}>Not Ekle</Text>
                            </Block>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={()=> Alert.alert(   
                                'Takım Sil'
                                ,'Takımı silmek istediğinizden emin misiniz?'
                                ,[
                                {text: 'Evet', onPress: () => this._deleteCategory(data.item.key) },
                                {text: 'Hayır'}
                                ]
                            )}
                        >
                            <Block right style={{justifyContent:'center', alignItems:'center', width: 100, borderRadius: 15, backgroundColor:'rgba(255, 0, 0, 0.8)'}}>
                                <Icon size={25} color="#FFF" style={{ marginLeft: 25 }} name="trash" family="font-awesome" />
                                <Text color="white" style={{ marginLeft: 25 }}>Sil</Text>
                            </Block>
                        </TouchableWithoutFeedback>
                    </Block>
                )}
                leftOpenValue={75}
                rightOpenValue={-75}
                />
            );
        }else if(this.state.categories.length != 0 && this.state.search == '') {
            return (
                <SwipeListView
                showsVerticalScrollIndicator={false}
                //disableRightSwipe={true}
                ListFooterComponent={this.state.item < this.state.categories.length && this.state.filteredCategories.length == 0 ? 
                    <Block center>
                        <Button onPress={this._increaseList} color="rgba(256, 256, 256, 0.3)" style={{ width: 100, height: 30, borderRadius: 15 }}>  Daha Fazla  </Button>
                    </Block>
                    :
                    <Block />
                }
                data={this.state.categories}
                renderItem={ (data, rowMap) => (
                    <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 0.25, y: 1.1 }}
                        locations={[0.2, 1]}
                        colors={['#addeff', '#98c3e0']}
                        style={styles.category}
                    >
                        <Block key={data.item.key}>
                            <Block style={styles.categoryTitle}>
                                <TouchableWithoutFeedback
                                    onPress={() => this._collapseActive(data.item.key)}
                                >
                                    <Block row space="between" style={{ marginVertical: theme.SIZES.BASE / 2 }}>
                                        <Block row>
                                            <Icon name="wrench" family="font-awesome" size={15} color="black" />
                                            <Text size={15} bold color={theme.COLORS.BLACK} style={{ paddingLeft:7 }}>{data.item.subPieceName}</Text>
                                        </Block>
                                        <Block row>
                                            {this.state.collapse[data.item.key] ? 
                                                <Icon name="angle-left" family="font-awesome" size={20} color="black"/>
                                                :
                                                <Icon name="angle-down" family="font-awesome" size={20} color="black"/>
                                            }
                                        </Block>
                                    </Block>
                                </TouchableWithoutFeedback>
                                <Divider style={{backgroundColor:'black'}}/>
                                {this.state.collapse[data.item.key] ? 
                                    <Block >
                                        <Block style={{ marginVertical: theme.SIZES.BASE / 2 }}>
                                            {data.item.type ? 
                                                <Text size={17} color={theme.COLORS.BLACK} >{data.item.pieceCount} Dakika</Text>
                                            :
                                                <Text size={17} color={theme.COLORS.BLACK} >{data.item.pieceCount}. Parça</Text>
                                            }
                                        </Block>
                                        <Block style={{ marginVertical: theme.SIZES.BASE / 2 }}>
                                            <SegmentedProgressBar
                                                values={[0, data.item.toolLife*0.33 + 1, data.item.toolLife*0.66 + 1, data.item.toolLife]}
                                                colors={['green', 'orange', 'red']}
                                                labels={['iyi','normal','kritik']}
                                                position={data.item.pieceCount}
                                            />
                                        </Block>
                                    </Block>
                                    :
                                    <Block >
                                        <Block center style={{ marginVertical: theme.SIZES.BASE / 2 }}>
                                            {this.state.subCategoryDetail.length == 0 ? 
                                            <Text size={25} color={theme.COLORS.BLACK} >Yükleniyor...</Text>
                                            :
                                            <Text size={20} color={theme.COLORS.BLACK} >Son {this.state.subCategoryDetail.length} Gün</Text>
                                            }
                                        </Block>
                                        {this.state.subCategoryDetail === [] ? 
                                        <ActivityIndicator animating={true} color={Colors.white} style={{alignSelf:'center', marginBottom: 10}}/>
                                        :
                                        <Block style={{marginLeft: -20, marginTop: -30}}>
                                            <VictoryChart
                                                // domainPadding will add space to each side of VictoryBar to
                                                // prevent it from overlapping the axis
                                                width={width}
                                                domainPadding={30}
                                                theme={VictoryTheme.material}
                                            >
                                                <VictoryAxis
                                                    // tickValues specifies both the number of ticks and where
                                                    // they are placed on the axis
                                                    tickValues={[1, 2, 3, 4, 5]}
                                                    tickFormat={(y) => (`${y}`)}
                                                    style={{
                                                        axis: {stroke: "rgba(56, 56, 56, 0.7)"},
                                                        axisLabel: {fontSize: 20, color:'rgba(56, 56, 56, 0.7)'},
                                                        ticks: {stroke: "rgba(56, 56, 56, 0.7)", size: 5},
                                                        tickLabels: {fontSize: 15, padding: 5, fill: 'rgba(56, 56, 56, 0.7)'}
                                                    }}
                                                />
                                                <VictoryAxis
                                                    dependentAxis
                                                    // tickFormat specifies how ticks should be displayed
                                                    tickFormat={(x) => (`${x}`)}
                                                    style={{
                                                        axis: {stroke: "rgba(25, 56, 56, 0.7)"},
                                                        axisLabel: {fontSize: 20, color:'rgba(56, 56, 56, 0.7)'},
                                                        ticks: {stroke: "rgba(56, 56, 56, 0.7)", size: 5},
                                                        tickLabels: {fontSize: 15, padding: 5, fill: 'rgba(56, 56, 56, 0.7)'}
                                                    }}
                                                />
                                                <VictoryBar
                                                    data={this.state.subCategoryDetail}
                                                    x="quarter"
                                                    y="earnings"
                                                    width= {10}
                                                    style= {{
                                                        data: { fill: 'rgba(56, 56, 56, 0.7)', width: 10 },
                                                        labels: { fill: 'rgba(56, 56, 56, 1)' }
                                                    }}
                                                    labelComponent={<VictoryLabel dx={15} dy={10}/>}
                                                />
                                            </VictoryChart>
                                        </Block>
                                        }
                                    </Block>
                                }
                            </Block>
                        </Block>
                    </LinearGradient>
                )}
                renderHiddenItem={ (data, rowMap) => (
                    <Block flex row style={[styles.hidden, {justifyContent:'space-between'}]}>
                        <TouchableWithoutFeedback
                            onPress={()=> this._noteModalActive(data.item.subPieceName,data.item.key)}
                        >
                            <Block left style={{justifyContent:'center', alignItems:'center', width: 100, borderRadius:15, backgroundColor:'rgba(0, 80, 255, 0.8)'}}>
                                <Icon size={25} color="#FFF" style={{ marginRight: 25 }} name="sticky-note" family="font-awesome" />
                                <Text color="white" style={{ marginRight: 25 }}>Not Ekle</Text>
                            </Block>
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback
                            onPress={()=> Alert.alert(   
                                'Takım Sil'
                                ,'Takımı silmek istediğinizden emin misiniz?'
                                ,[
                                {text: 'Evet', onPress: () => this._deleteCategory(data.item.key) },
                                {text: 'Hayır'}
                                ]
                            )}
                        >
                            <Block right style={{justifyContent:'center', alignItems:'center', width: 100, borderRadius: 15, backgroundColor:'rgba(255, 0, 0, 0.8)'}}>
                                <Icon size={25} color="#FFF" style={{ marginLeft: 25 }} name="trash" family="font-awesome" />
                                <Text color="white" style={{ marginLeft: 25 }}>Sil</Text>
                            </Block>
                        </TouchableWithoutFeedback>
                    </Block>
                )}
                leftOpenValue={75}
                rightOpenValue={-75}
                />
            );
        }else{
            return (
                <Block style={styles.emty}>
                    <Icon name="info-circle" family="font-awesome" size={50} color="white" style={{ marginVertical: 5, marginTop: theme.SIZES.BASE * 10}}/>
                    <Text style={{color:'white'}}>Herhangi bir parça bulunamadı.</Text>
                </Block>
            )
        }
    }

    render() {
        const { open } = this.state;

        if (this.state.isLoading) {
            return (
                <Loading />
            )
        }
        return (
            <LinearGradient
				start={{ x: 0, y: 0 }}
				end={{ x: 0.25, y: 1.1 }}
				locations={[0.2, 1]}
				colors={['#3ca4d5', '#003249']}
				style={[styles.home, {flex: 1, paddingTop: theme.SIZES.BASE * 5}]}
            >
                <Block row>
                    {this._renderSearch()}
                    <Block style={{justifyContent:'center', alignItems:'center'}}>
                        <NotificationIcon />
                    </Block>
                </Block>
                {this._renderFilter()}
                {this._renderCategories()}
                <Modal isVisible={this.state.formModalVisible}>
                    <AddForm 
                    formModalActive = {this._formModalActive} 
                    pieceID = {this.props.navigation.getParam('pieceId')}
                    retrieveCategories = {this._retrieveCategories}
                    />
                </Modal>
                <Modal isVisible={this.state.noteFormVisible}>
                    <AddNewNote noteFormVisible = {this._noteModalActive} retrieveCategories = {this._retrieveCategories} activeSubPieceName={this.state.activeSubPieceName} activeSubPieceId={this.state.activeSubPieceId}/>
                </Modal>
                <Provider>
                    <Portal>
                        <FAB.Group
                            open={open}
                            icon={open ? 'close' : 'pencil'}
                            actions={[
                                { icon: 'help', label:'Yardım', onPress: () => NavigationService.navigate('Help') },
                                { icon: 'plus', label:'Parça Ekle', onPress: () => this._formModalActive() },
                                { icon: 'file', label:'Rapor', onPress: () => NavigationService.navigate('Report') },
                            ]}
                            onStateChange={this._onStateChange}
                            onPress={() => {
                            if (open) {
                                // do something if the speed dial is open
                            }
                            }}
                        />
                    </Portal>
                </Provider>
            </LinearGradient>
        )
    }
}

const styles = StyleSheet.create({
    home: {
        marginTop: Platform.OS === 'android' ? -theme.SIZES.BASE*2 : 0,
    },
    categoryList: {
      justifyContent: 'center',
      paddingTop: theme.SIZES.BASE * 0.01,
      paddingBottom: 100
    },
    category: {
        backgroundColor: 'transparent',
        marginHorizontal: theme.SIZES.BASE,
        marginVertical: theme.SIZES.BASE / 2,
        borderRadius: 15,
        borderWidth: 0,
    },
    categoryTitle: {
      paddingHorizontal: theme.SIZES.BASE,
      backgroundColor: 'rgba(0, 0, 0, 0.01)',
    },
    emty: {
      paddingHorizontal: theme.SIZES.BASE,
      backgroundColor: 'rgba(0, 0, 0, 0.01)',
      alignItems: 'center'
    },
    search: {
        height: 40,
        width: width - 75,
        marginLeft: theme.SIZES.BASE ,
        marginRight: theme.SIZES.BASE / 2,
        backgroundColor: 'rgba(255, 255, 255, 0.7)',
        borderWidth: 0,
        borderRadius: 15,
    },
    shadowSearch: {
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 3 },
        shadowRadius: 4,
        shadowOpacity: 0.1,
        elevation: 2,
    },
    filter: {
        width: (width - 47) / 4,
        height: 20,
        marginRight: 5,
        marginVertical: theme.SIZES.BASE / 2,
    },
    hidden: {
        flex: 1,
        backgroundColor: 'transparent',
        marginHorizontal: theme.SIZES.BASE,
        marginVertical: theme.SIZES.BASE / 2,
        borderRadius: 15,
        borderWidth: 0,
    },
  });
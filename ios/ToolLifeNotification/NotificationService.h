//
//  NotificationService.h
//  ToolLifeNotification
//
//  Created by Özer Cem Kelahmet on 16.07.2020.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
